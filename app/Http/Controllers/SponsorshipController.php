<?php

namespace App\Http\Controllers;

use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use App\Models\Saving;
use App\Models\User;
use Illuminate\Http\Request;
use App\Core\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SponsorshipController extends Controller
{
    public function getSponsorshipBonusTree()
    {
        try {
            Verification::check(Constants::USER_ID);
            $res = array();
            $res['parent'] = DB::table(Constants::USERS)
            ->select(
                Constants::NAME,
                Constants::USER_ID,
                Constants::PICTURE,
                Constants::MEMBER_DETAILS.".".Constants::ID
                )
            ->join(Constants::MEMBER_DETAILS,Constants::SALT,"=",Constants::USER_ID)
            ->where(Constants::SALT,Input::get(Constants::USER_ID))
            ->first();

            $res['child'] = DB::table(Constants::SPONSORSHIPS)
            ->select(
                Constants::USERS.".".Constants::NAME,
                Constants::REFERENCES.".".Constants::USER_ID,
                Constants::PICTURE,
                Constants::MEMBER_DETAILS.".".Constants::ID,
                Constants::REFERENCES.".".Constants::REF_NO
                )
            ->join(Constants::REFERENCES,Constants::REFERENCES.".".Constants::ID,"=",Constants::SPONSORSHIPS.".".Constants::REF_ID)
            ->join(Constants::MEMBER_DETAILS,Constants::REFERENCES.".".Constants::USER_ID,"=",Constants::MEMBER_DETAILS.".".Constants::USER_ID)
            ->join(Constants::USERS,Constants::REFERENCES.".".Constants::USER_ID,"=",Constants::USERS.".".Constants::SALT)
            ->orderBy(Constants::REFERENCES.".".Constants::REF_NO,"ASC")
            ->where(Constants::PARENT_ID,Input::get(Constants::USER_ID))
            ->get();

            return Response::success($res);
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

}
