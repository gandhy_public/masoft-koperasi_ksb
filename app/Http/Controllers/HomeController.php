<?php

namespace App\Http\Controllers;

use App\Core\Constants;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function dashboard()
    {
        session_start();
        if (!isset($_SESSION[Constants::SESSION_USER_ID])) {
            return redirect()->route('login');
        } else {
            $title = 'Dashboard';
            return view('page.dashboard', compact('title'));
        }
    }
}
