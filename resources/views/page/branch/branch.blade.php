@extends('templates.index')
@section('title')
    Simpanan Wajib dan Pokok
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Branch</h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:">Home</a>
            </li>
            <li>
                <a href="javascript:">Setting</a>
            </li>
            <li class="active">Branch</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Tabel Branches</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons" >
                            <button id="add_branch" class="btn btn-transparent dark btn-outline btn-circle btn-sm active">Tambah</button>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="branch_table">
                        <thead>
                        <tr>
                            <th> Kode Cabang </th>
                            <th> Nama Cabang </th>
                            <th> Kode POS </th>
                            <th> Status </th>
                            <th> Created At </th>
                            <th> Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>





    <!-- MODAL TAMBAH BRANCH -->
    <div class="modal fade bs-modal-lg" id="branch_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="modal_judul">Pendaftaran Cabang Baru</h4>
                </div>
                <form id="form" class="form-horizontal">
                    <input type="hidden" name="branch_id" id="input_id" value="">
                    <input type="hidden" name="branch_code_asli" id="input_code_asli" value="">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group modal-logo" align="center">
                            
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama Cabang</label>
                                <div class="col-md-9">
                                    <input type="text" id="input_name" class="form-control" name="branch_name" placeholder="Nama Cabang" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alamat Cabang</label>
                                <div class="col-md-9">
                                        <textarea id="input_address" name="branch_address" class="form-control" rows="3" placeholder="Alamat Cabang" value=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Kode Pos</label>
                                <div class="col-md-9">
                                    <input type="number" id="input_pos" class="form-control" name="branch_pos" placeholder="Kode Pos" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Telepon</label>
                                <div class="col-md-9">
                                    <input type="text" id="input_phone" class="form-control" name="branch_phone" placeholder="+62xxx" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Kode Cabang</label>
                                <div class="col-md-9">
                                    <input type="text" id="input_code" class="form-control" name="branch_code" placeholder="Kode Cabang" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Bank</label>
                                <div class="col-md-9 bank">
                                    <select id="input_bank" class="form-control select2" name="branch_bank">
                                            <option value="---">Loading...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama Rekening Bank</label>
                                <div class="col-md-9">
                                    <input type="text" id="input_bank_name" class="form-control" name="branch_bank_name" placeholder="Nama Pemilik Rekening Bank" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Rekening Bank</label>
                                <div class="col-md-9">
                                    <input type="text" id="input_bank_number" class="form-control" name="branch_bank_number" placeholder="Nomor Rekening Bank" value="">
                                </div>
                            </div>
                            <div class="form-group logo">
                                <label class="col-md-3 control-label">Logo Cabang</label>
                                <div class="col-md-9">
                                    <input type="button" x-data-file-name="" id="input_logo" class="form-control btn btn-primary" name="logo" value="Upload file">
                                </div>
                                <input id="import-input" type="file"  name="name" style="display: none;" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" id="modal_button_close" data-dismiss="modal">Close</button>
                        <button type="button" class="modal-button btn green" id="modal_button_save">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL TAMBAH BRANCH SELESAI -->
@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){


            /*READ DATA*/
            //Menampilkan data ke tabel
            var table=  $('#branch_table').DataTable({
                dom: "Bfrtip",
                ajax: {
                    "url":"{{ url('/api/v1/getTableBranch') }}",
                    "type":"POST"
                },
                columns: [
                    { data: "code" },
                    { data: "name" },
                    { data: "postal_code"},
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            if (full.status === "disabled") {
                                res += "<h4 align='center'><span class=\'label label-warning\'>" + full.status + "</span></h4>";
                            }
                            else if (full.status === "active") {
                                res += "<h4 align='center'><span class=\'label label-success\'>" + full.status + "</span></h4>";
                            }
                            else if (full.status === "closed") {
                                res += "<h4 align='center'><span class=\'label label-danger\'>" + full.status + "</span></h4>";
                            }
                            else {
                                res += "<h4 align='center'><span class=\'label label-primary\'>" + full.status + "</span></h4>";
                            }
                            return res;
                        }
                    },
                    { data: "created_at"},
                    { sortable: false,
                        "render": function ( data, type, full, meta ) {
                            var res = "";
                            res +=  "<a href=\"javascript:;\" id=\"view_"+full.id+"\" class=\"btn-view fa fa-eye\" role=\"button\" x-data=\"" + JSON.stringify(full).replace(/\"/g,"'") + "\"></a> ";
                            res +=  "<a href=\"javascript:;\" id=\"edit_"+full.id+"\" class=\"btn-edit fa fa-pencil\" role=\"button\"  x-data=\"" + JSON.stringify(full).replace(/\"/g,"'") + "\"></a> ";
                            res +=  "<a href=\"javascript:;\" id=\"delete_"+full.id+"\" class=\"btn-delete fa fa-trash\" role=\"button\"  x-data=\"" + full.id + "\"></a>";
                            return res;
                        }
                    }
                ],
                select: true,
                buttons:[]
            });
            //Menampilkan detail data
            $('#branch_table').on("click",".btn-view",function(){
                //alert('ini detail branch');
                var json_data = JSON.parse($(this).attr("x-data").replace(/\'/g,"\""));
                $("#branch_modal").modal("show");
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getBranchDetails') !!}",
                    dataType: 'json',
                    data:{
                        branch_id:json_data.id
                    },
                    success : function(res){
                        bank_now = res.data.branch_bank_id;
                        $('#modal_judul').text("Detail Data Cabang");
                        view_logo = '<img src="<?php echo "http://" . $_SERVER['HTTP_HOST'] . "/uploads/branches/"?>' + res.data.branch_logo + '" style="width: 30%;height: 30%">';
                        $(".modal-logo").html(view_logo);

                        $("#input_name").val(res.data.branch_name).attr("disabled", true);
                        $('#input_address').val(res.data.branch_address).attr("disabled", true);
                        $("#input_pos").val(res.data.branch_postal).attr("disabled", true);
                        $("#input_phone").val(res.data.branch_phone).attr("disabled", true);
                        $("#input_code").val(res.data.branch_code).attr("disabled", true);
                        $("#input_bank_name").val(res.data.branch_bank_name).attr("disabled", true);
                        $("#input_bank_number").val(res.data.branch_bank_rekening).attr("disabled", true);
                        view_bank = '<input type="text" id="input_bank_number" class="form-control" name="branch_bank_number" placeholder="Nomor Rekening Bank" value="Bank Central Asia" disabled>';
                        $(".bank").html(view_bank);
                        $(".logo").hide("");
                        $(".modal-button").hide();
                        
                    }
                });
            });



            /*ADD DATA*/
            //Menampilkan modal form jika button tambah diclick
            $("#add_branch").click(function(){
                view_logo = '';
                $(".modal-logo").html(view_logo);

                $('#modal_judul').text("Pendaftaran Cabang Baru");
                $("#input_id").val("");
                $("#input_code_asli").val("");

                $("#input_name").val("").attr("disabled", false);
                $('#input_address').val("").attr("disabled", false);
                $("#input_pos").val("").attr("disabled", false);
                $("#input_phone").val("").attr("disabled", false);
                $("#input_code").val("").attr("disabled", false);
                view_bank = '<select id="input_bank" class="form-control select2" name="branch_bank"><option value="---">Loading...</option></select>';
                $(".bank").html(view_bank);
                $("#input_bank_name").val("").attr("disabled", false);
                $("#input_bank_number").val("").attr("disabled", false);
                $(".logo").show("");

                $(".modal-button").show();
                $(".modal-button").attr("id","modal_button_save");
                $(".modal-button").text("Save changes");
                $("#branch_modal").modal("show");
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getBranchBank') !!}",
                    dataType: 'json',
                    success : function(res){
                        banks = "";
                        for(var i = 0;i<res.data.length;i++){
                            banks += '<option value="' + res.data[i].id + '">' + res.data[i].full_name + '</option>'
                        }
                        $('#input_bank').html(banks);
                    }
                });
            });
            //Menampilkan direktori file saat button upload diclick
            $("#input_logo").click(function(){
                $('#import-input').trigger('click');
            });
            //Melakukan upload file ke server jika sudah memilih filenya
            $("#import-input").change(function(){
                $("#branch_modal").modal("hide");
                swal({
                    title: 'Importing!',
                    text: 'File sedang di upload, mohon tunggu dan jangan reload/refresh halaman ini!',
                    timer: 4000,
                    showCancelButton: false,
                    showConfirmButton: false
                }).then(
                    function(){
                        var filex = $("#import-input")[0].files[0];
                        var fd = new FormData();
                        fd.append('file', filex);
                        $.ajax({
                            type: 'POST',
                            url: "{!! url('/api/v1/uploadBranchLogo')!!}",
                            data: fd,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success : function(res){
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    title = "Imported!";
                                    content = res.data.message;
                                    tag = "success";
                                    $("#input_logo").attr("x-data-file-name",res.data.file_name);
                                    table.ajax.reload( null, false );
                                }
                                swal({
                                    title:title,
                                    text:content,
                                    type:tag
                                }).then(function(){
                                    $("#branch_modal").modal("show");
                                });
                            }
                        });
                    },
                    function(dismiss){
                        if (dismiss === 'timer') {
                            //console.log('I was closed by the timer')
                        }
                    }
                )
            });
            //Melakukan penyimpanan semua data form ke database
            $('#branch_modal').on("click","#modal_button_save",function(){
                //alert("he");
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/addBranch') !!}",
                    dataType: 'json',
                    data:{
                        name:$("#input_name").val(),
                        address:$("#input_address").val(),
                        pos:$("#input_pos").val(),
                        phone:$("#input_phone").val(),
                        code:$("#input_code").val(),
                        bank_id:$("#input_bank").val(),
                        bank_name:$("#input_bank_name").val(),
                        bank_number:$("#input_bank_number").val(),
                        logo:$("#input_logo").attr("x-data-file-name")
                    },
                    success : function(res){
                        $("#branch_modal").modal("hide");
                        var title = "Error!";
                        var content = res.message;
                        var tag = "error";
                        if (res.status === 1) {
                            title = "Berhasil Tambah!";
                            content = res.data.message;
                            tag = "success";
                            table.ajax.reload( null, false );
                            swal({
                                title:title,
                                text:content,
                                type:tag
                            }).then(function(){
                                table.ajax.reload( null, false );
                                //window.location.reload();
                                $("#input_name").val("");
                                $('#input_address').val("");
                                $("#input_pos").val("");
                                $("#input_phone").val("");
                                $("#input_code").val("");
                                $("#input_code").val("");
                                $("#input_bank_name").val("");
                                $("#input_bank_number").val("");
                            });
                        }else{
                            swal({
                                title:title,
                                text:content,
                                type:tag
                            }).then(function(){
                                $("#branch_modal").modal("show");
                            });
                        }
                    }
                });
            });



            /*DELETE DATA*/
            //Melakukan hapus data jika icon sampah diclik
            $('#branch_table').on("click",".btn-delete",function () {
                var branch_id = $(this).attr("x-data").replace(/\'/g,"\"");
                swal({
                    title: "Apakah Anda yakin ingin menghapus data ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'POST',
                            url: "{!! url('/api/v1/deleteBranch') !!}",
                            dataType: 'json',
                            data:{
                                branch_id:branch_id
                            },
                            success : function(res){
                                $("#branch_modal").modal("hide");
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    table.ajax.reload( null, false );
                                    swal("Data berhasil dihapus!", {
                                      icon: "success",
                                    }).then(function(){
                                        table.ajax.reload( null, false );
                                        //window.location.reload();
                                    });
                                }else{
                                    swal({
                                        title:title,
                                        text:content,
                                        type:tag
                                    });
                                }
                            }
                        });
                    }
                });
            });



            /*UPDATE DATA*/
            //Menampilkan form edit data
            $('#branch_table').on("click",".btn-edit",function () {
                var json_data = JSON.parse($(this).attr("x-data").replace(/\'/g,"\""));
                $("#branch_modal").modal("show");
                var bank_now = "";
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getBranchDetails') !!}",
                    dataType: 'json',
                    data:{
                        branch_id:json_data.id
                    },
                    success : function(res){
                        bank_now = res.data.branch_bank_id;
                        view_logo = '';
                        $(".modal-logo").html(view_logo);
                        $('#modal_judul').text("Update Data Cabang");
                        $("#input_id").val(json_data.id);
                        $("#input_code_asli").val(res.data.branch_code);

                        $("#input_name").val(res.data.branch_name).attr("disabled", false);
                        $('#input_address').val(res.data.branch_address).attr("disabled", false);
                        $("#input_pos").val(res.data.branch_postal).attr("disabled", false);
                        $("#input_phone").val(res.data.branch_phone).attr("disabled", false);
                        $("#input_code").val(res.data.branch_code).attr("disabled", false);
                        view_bank = '<select id="input_bank" class="form-control select2" name="branch_bank"><option value="---">Loading...</option></select>';
                        $(".bank").html(view_bank);
                        $("#input_bank_name").val(res.data.branch_bank_name).attr("disabled", false);
                        $("#input_bank_number").val(res.data.branch_bank_rekening).attr("disabled", false);

                        $(".logo").show("");
                        $(".modal-button").show();
                        $(".modal-button").attr("id","update_input_save");
                        $(".modal-button").text("Update changes");
                        
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getBranchBank') !!}",
                    dataType: 'json',
                    success : function(res){
                        banks = "";
                        for(var i = 0;i<res.data.length;i++){
                            var selected_bank = bank_now == res.data[i].id?"selected":"";
                            banks += '<option value="' + res.data[i].id + '" ' + selected_bank + '>' + res.data[i].full_name + '</option>'
                        }
                        $('#input_bank').html(banks);
                    }
                });
            });
            //Melakukan edit data
            $('#branch_modal').on("click","#update_input_save",function(){
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/updateBranch') !!}",
                    dataType: 'json',
                    data:{
                        id:$("#input_id").val(),
                        code_asli:$("#input_code_asli").val(),

                        name:$("#input_name").val(),
                        address:$("#input_address").val(),
                        pos:$("#input_pos").val(),
                        phone:$("#input_phone").val(),
                        code:$("#input_code").val(),
                        bank_id:$("#input_bank").val(),
                        bank_name:$("#input_bank_name").val(),
                        bank_number:$("#input_bank_number").val(),
                        logo:$("#input_logo").attr("x-data-file-name")
                    },
                    success : function(res){
                        $("#branch_modal").modal("hide");
                        var title = "Error!";
                        var content = res.message;
                        var tag = "error";
                        if (res.status === 1) {
                            title = "Berhasil Update Data!";
                            content = res.data.message;
                            tag = "success";
                            table.ajax.reload( null, false );
                            swal({
                                title:title,
                                text:content,
                                type:tag
                            }).then(function(){
                                table.ajax.reload( null, false );
                                //window.location.reload();
                                $("#input_name").val("");
                                $('#input_address').val("");
                                $("#input_pos").val("");
                                $("#input_phone").val("");
                                $("#input_code").val("");
                                $("#input_code").val("");
                                $("#input_bank_name").val("");
                                $("#input_bank_number").val("");

                                $('#modal_judul').text("Pendaftaran Cabang Baru");
                                $("#input_id").val("");
                                $("#input_code_asli").val("");

                                $("#input_name").val("");
                                $('#input_address').val("");
                                $("#input_pos").val("");
                                $("#input_phone").val("");
                                $("#input_code").val("");
                                $("#input_bank_name").val("");
                                $("#input_bank_number").val("");

                                $(".modal-button").attr("id","modal_button_save");
                                $(".modal-button").text("Save changes");
                            });
                        }else{
                            swal({
                                title:title,
                                text:content,
                                type:tag
                            }).then(function(){
                                $("#branch_modal").modal("show");
                            });
                        }
                    }
                });
            });


        });
    </script>
@endsection