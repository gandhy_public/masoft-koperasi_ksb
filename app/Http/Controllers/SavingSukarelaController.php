<?php

namespace App\Http\Controllers;

use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SavingSukarelaController extends Controller
{
    public function getMembers()
    {
        try {
            $data = DB::table('users');
            $data->select(
                'users.salt as user_id',
                'member_details.identity_num as user_ktp',
                'users.name as user_name')
                ->where('users.role', 'Member')
                ->join('member_details', 'users.salt', '=', 'member_details.user_id');
            return Response::table($data->get());
        } catch (VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            //return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getSavingSukarela()
    {
        try {
            Verification::check('user_id', 'User_ID tidak boleh kosong!');
            $data = DB::table('savings');

            $data->select(
                'created_at as tanggal',
                'amount')
                ->where('type', 'sukarela')
                ->where('user_id', Input::get('user_id'))
                ->where('created_at', 'like', date('Y-m%'));
            return Response::table($data->get());
        } catch (VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            //return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function addSavingSukarela()
    {
        try {
            Verification::check('user_id', 'User_ID tidak boleh kosong');
            Verification::check('amount', 'Jumlah Simpanan tidak boleh kosong');
            $data = DB::table('savings');

            if ($_SESSION['ksbrole'] != "Member") {
                $data->insert([
                    'user_id' => Input::get('user_id'),
                    'amount' => Input::get("amount"),
                    'status' => "paid",
                    'type' => "sukarela",
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                return Response::success("Simpanan Sukarela Anda berhasil tersimpan!");
            } else {
                throw new VerificationException("role tidak diizinkan!");
            }
        } catch (VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            return Response::failed($e->getMessage());
            //return Response::failed(Constants::SERVER_FAILURE);
        }
    }
}