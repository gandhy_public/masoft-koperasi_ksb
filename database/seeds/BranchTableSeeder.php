<?php

use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('branches')->insert(
            [
                'id'=>'1',
                'name' =>  "Keluarga Sejahtera Bersama Tangerang",
                'address' => 'Jl rawa buntu no 12, Neglasari ,Tangerang, Banten.',
                'postal_code' => "15127",
                'phone' => "021233123",
                'logo' => "no-image.jpg",
                'status' => "active",
                'code' => "KSBTANG1",
                'created_at' => date('Y-m-d H:i:s')

            ]
        );
    }
}
