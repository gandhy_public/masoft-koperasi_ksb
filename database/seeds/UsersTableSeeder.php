<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $username = "member";
        $password = "member";
        DB::table('users')->insert(
            [
                'id'=>'1',
                'username' => $username,
                'password' => password_hash($password,PASSWORD_BCRYPT),
                'email' => $username . "@ksb.com",
                'name' => $username,
                'role' => 'Member',
                'balance' => 10000000,
                'phone' => '085848584561',
                'address' => 'Jl. Sawojajar',
                'salt' => '$49213jfdsajkk.4KSKJTestSalt1',
            ]);
        DB::table('member_details')->insert(
            [
                'id'=>'1',
                'user_id'=>'$49213jfdsajkk.4KSKJTestSalt1',
                'ref_quota' => 5,
                'ref_code' => "member2605",
                'birth_place' => $username,
                'birth_date' => "1999-05-26",
                'identity_num' => 3577452134564,
                'identity_type' => 'ktp',
                'mother_name' => 'Ibu',
                'first_heir' => 'Anak 1',
                'second_heir' => 'Anak 2',
                'job_name' => 'Petani disawah',
                'branch_id' => '1',
                'type' => 'member',
            ]);

        DB::table('users')->insert(
            [
                'id'=>'2',
                'username' => 'admin',
                'password' => password_hash('admin',PASSWORD_BCRYPT),
                'email' => 'admin' . "@ksb.com",
                'name' => 'admin',
                'role' => 'Admin',
                'phone' => '085848584561',
                'address' => 'Jl. Sawojajar',
                'salt' => '$49213jfdsajkk.4KSKJTestSalt2',
                'created_at' => date('Y-m-d H:i:s')
            ]);
        DB::table('users')->insert(
            [
                'id'=>'3',
                'username' => 'suadmin',
                'password' => password_hash('suadmin',PASSWORD_BCRYPT),
                'email' => 'suadmin' . "@ksb.com",
                'name' => 'suadmin',
                'role' => 'Super Admin',
                'phone' => '085848584561',
                'address' => 'Jl. Sawojajar',
                'salt' => '$49213jfdsajkk.4KSKJTestSalt3',
                'created_at' => date('Y-m-d H:i:s')
            ]);
    }
}
