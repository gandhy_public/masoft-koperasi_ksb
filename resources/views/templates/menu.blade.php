<header class="page-header">
    <nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid">
            <div class="clearfix navbar-fixed-top" style="z-index: 99;">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                </button>

                <a id="index" class="page-logo" href=""><img src="{{ asset('assets/layouts/layout5/img/logo.png') }}" alt="Logo"></a>

                <div class="topbar-actions">
                    <div class="btn-group-red btn-group">
                        <button type="button" class="btn btn-sm"><i class="fa fa-plus"></i></button>
                    </div>

                    <div class="btn-group-img btn-group">
                        <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span>Hi, {{ $_SESSION[\App\Core\Constants::NAME] }}</span>
                            <img src="{{ asset('assets/layouts/layout5/img/avatar1.jpg') }}" alt=""> </button>
                        <ul class="dropdown-menu-v2" role="menu">
                            <li><a href=""><i class="icon-user"></i> My Profile</a></li>
                            <li><a href="{{ route('logout') }}"><i class="icon-key"></i> Log Out </a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="{{ route('home') }}" class="text-uppercase"><i class="icon-home"></i> Dashboard </a></li>
                    <li class="dropdown dropdown-fw"><a href="javascript:" class="text-uppercase"><i
                                    class="icon-users"></i> Anggota <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            @if($_SESSION[\App\Core\Constants::SESSION_ROLE] == 'Admin' || $_SESSION[\App\Core\Constants::SESSION_ROLE] == "Super Admin")
                                <li><a href="{{ route('anggota') }}"><i class="fa fa-chevron-circle-right"></i> Data Anggota </a></li>
                                <li><a href="{{ url('bonus/bulanan') }}"><i class="fa fa-chevron-circle-right"></i> Bonus Bulanan </a>
                                </li>
                                <li><a href="{{ url('bonus/perekrutan') }}"><i class="fa fa-chevron-circle-right"></i> Bonus Perekrutan </a></li>
                            @else
                                <li><a href="{{ url('bonus/bulanan') }}"><i class="fa fa-chevron-circle-right"></i> Bonus Bulanan </a>
                                </li>
                                <li><a href="{{ url('bonus/perekrutan') }}"><i class="fa fa-chevron-circle-right"></i> Bonus Perekrutan </a></li>
                            @endif
                        </ul>
                    </li>
                    <li class="dropdown dropdown-fw"><a href="javascript:" class="text-uppercase"><i
                                    class="icon-doc"></i> Brosur <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li><a href="javascript:"><i class="fa fa-chevron-circle-right"></i> Brosur </a></li>
                            <li><a href="javascript:"><i class="fa fa-chevron-circle-right"></i> Perspektif </a></li>
                            <li><a href="javascript:"><i class="fa fa-chevron-circle-right"></i> Pengumuman </a></li>
                        </ul>
                    </li>
                    <li class=""><a href="javascript:" class="text-uppercase"><i class="icon-note"></i> Report </a></li>
                    <li class="dropdown dropdown-fw"><a href="javascript:" class="text-uppercase"><i
                                    class="icon-wallet"></i> Simpan Pinjam <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu dropdown-menu-fw">
                            <li><a href="javascript:"><i class="fa fa-chevron-circle-right"></i> Simpanan Wajib dan
                                    Pokok </a></li>
                            <li><a href="{{ url('/simpanan/sukarela') }}"><i class="fa fa-chevron-circle-right"></i>
                                    Simpanan Sukarela </a>
                            </li>
                            <li><a href="{{ url('/pinjaman') }}"><i class="fa fa-chevron-circle-right"></i> Pinjaman
                                </a></li>
                        </ul>
                    </li>
                    <li class=""><a href="javascript:" class="text-uppercase"><i class="icon-handbag"></i> Toko </a>
                    </li>
                    @if($_SESSION[\App\Core\Constants::SESSION_ROLE] == 'Super Admin')
                        <li class=""><a href="{{ url('setting/branch') }}" class="text-uppercase"><i
                                        class="icon-settings"></i> Setting </a>
                        </li>
                    @else
                        <li class=""><a href="javascript:" class="text-uppercase"><i class="icon-settings"></i> Setting
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>