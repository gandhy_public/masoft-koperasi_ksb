<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMemberDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->unique();
            $table->string('approved_by')->nullable();
            $table->string('ref_code');
            $table->integer('ref_quota')->default(5);
            $table->string('birth_place');
            $table->date('birth_date');
            $table->string('identity_num');
            $table->string('identity_type');
            $table->string('mother_name');
            $table->string('first_heir');
            $table->string('second_heir');
            $table->string('job_name');
            $table->string('branch_id');
            $table->enum('type', ['koperasi', 'member']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_details');
    }
}
