@extends('templates.index')
@section('title')
    Pinjaman
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Pinjaman</h1>
        <ol class="breadcrumb">
            <li>
                <a href="javascript:">Home</a>
            </li>
            <li>
                <a href="javascript:">Simpan Pinjam</a>
            </li>
            <li class="active">Pinjaman</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Tabel Pinjaman</span>
                    </div>
                    <?php if($_SESSION['ksbrole'] == "Member"){?>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <button id="add_loan"
                                    class="btn btn-transparent dark btn-outline btn-circle btn-sm active">Request
                                Pinjaman
                            </button>
                        </div>
                    </div>
                    <?php }else {
                    }?>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="loan_table">
                        <thead>
                        <?php if($_SESSION['ksbrole'] == "Member"){?>
                        <tr>
                            <th> Tanggal Pinjam</th>
                            <th> Jumlah Pinjaman</th>
                            <th> Status</th>
                            <th> Nomor Pinjaman</th>
                        </tr>
                        <?php }else{?>
                        <tr>
                            <th> Tanggal Pinjam</th>
                            <th> Member</th>
                            <th> Jumlah Pinjaman</th>
                            <th> Status</th>
                            <th> Nomor Pinjaman</th>
                            <th> Aksi</th>
                        </tr>
                        <?php }?>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>





    <!-- MODAL TAMBAH BRANCH -->
    <div class="modal fade bs-modal-lg" id="loan_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="modal_judul">Request Pinjaman</h4>
                </div>
                <form id="form" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jumlah Pinjaman</label>
                                <div class="col-md-9">
                                    <select id="input_amount" class="form-control select2" name="loan_amount">
                                        <option value="500000">Rp. 500.000</option>
                                        <option value="1000000">Rp. 1.000.000</option>
                                        <option value="1500000">Rp. 1.500.000</option>
                                        <option value="2000000">Rp. 2.000.000</option>
                                        <option value="2500000">Rp. 2.500.000</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" id="modal_button_close" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="modal-button btn green" id="modal_button_save">Request
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL TAMBAH BRANCH SELESAI -->
@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function () {


            /*READ DATA*/
            //Menampilkan data ke tabel
            var table = $('#loan_table').DataTable({
                dom: "Bfrtip",
                ajax: {
                    "url": "{{ route('get.loans') }}",
                    "type": "POST"
                },
                columns: [
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<h4 align='center'>" + full.created_at.substr(0, 10) + "</h4>";
                            return res;
                        }
                    },
                        <?php if ($_SESSION['ksbrole'] != "Member"){?>
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<h4 align='center'>" + full.name + "</h4>";
                            return res;
                        }
                    },
                        <?php }?>
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<h4 align='center'>Rp. " + full.amount + "</h4>";
                            return res;
                        }
                    },
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            if (full.status === "pending") {
                                res += "<h4 align='center'><span class=\'label label-warning\'>" + full.status + "</span></h4>";
                            }
                            else if (full.status === "approved") {
                                res += "<h4 align='center'><span class=\'label label-success\'>" + full.status + "</span></h4>";
                            }
                            else if (full.status === "rejected") {
                                res += "<h4 align='center'><span class=\'label label-danger\'>" + full.status + "</span></h4>";
                            }
                            else {
                                res += "<h4 align='center'><span class=\'label label-primary\'>" + full.status + "</span></h4>";
                            }
                            return res;
                        }
                    },
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            if (full.nomor_pinjaman === "") {
                                res += "<h4 align='center'>Waiting Status Aproved</h4>";
                            }
                            else {
                                res += "<h4 align='center'>" + full.nomor_pinjaman + "</h4>";
                            }
                            return res;
                        }
                    },
                        <?php if($_SESSION['ksbrole'] != "Member"){?>
                    {
                        sortable: false,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            if (full.status === "pending") {
                                res += "<div align='center'>";
                                res += "<a href='javascript:;' id='approve_" + full.id + "' x-data='" + full.id + "' class=\"btn-approve btn btn-success\"><span class=\"glyphicon glyphicon-ok\"></span> Approve </a>";
                                res += "<a href='javascript:;' id='reject_" + full.id + "' x-data='" + full.id + "' class=\"btn-reject btn btn-danger\"><span class=\"glyphicon glyphicon-remove\"></span> Reject </a>";
                                res += "</div>";
                            } else if (full.status === "approved") {
                                res += "<div align='center'>";
                                res += "<a href='javascript:;' id='paid_" + full.id + "' x-data='" + full.id + "' class=\"btn-paid btn btn-default\"><span class=\"glyphicon glyphicon-check\"></span> Paid </a>";
                                res += "</div>";
                            }
                            return res;
                        }
                    }
                    <?php }?>
                ],
                select: true,
                buttons: []
            });

            /*ADD DATA*/
            //Menampilkan Modal Tambah Data
            $("#add_loan").click(function () {
                $("#loan_modal").modal("show");
            });
            //Melakukan penyimpanan semua data form ke database
            $('#loan_modal').on("click", "#modal_button_save", function () {
                $.ajax({
                    type: 'POST',
                    url: "{!! route('add.loans') !!}",
                    dataType: 'json',
                    data: {
                        amount: $("#input_amount").val()
                    },
                    success: function (res) {
                        $("#loan_modal").modal("hide");
                        var title = "Error!";
                        var content = res.message;
                        var tag = "error";
                        if (res.status === 1) {
                            title = "Berhasil Request!";
                            content = res.data.message;
                            tag = "success";
                            table.ajax.reload(null, false);
                            swal({
                                title: title,
                                text: content,
                                type: tag
                            }).then(function () {
                                table.ajax.reload(null, false);
                                //window.location.reload();
                                $("#input_amount").val("");
                            });
                        } else {
                            swal({
                                title: title,
                                text: content,
                                type: tag
                            }).then(function () {
                                $("#loan_modal").modal("show");
                            });
                        }
                    }
                });
            });

            /*APPROVE LOAN*/
            $('#loan_table').on("click", ".btn-approve", function () {
                var loan_id = $(this).attr('x-data').replace(/\'/g, "\"");
                swal({
                    title: "Apakah Anda yakin ingin menyetuji data pinjaman ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willApprove) => {
                    if (willApprove) {
                        $.ajax({
                            type: 'POST',
                            url: "{!! route('aksi.loans') !!}",
                            dataType: 'json',
                            data: {
                                loan_id: loan_id,
                                aksi: 'approve'
                            },
                            success: function (res) {
                                //$("#loans_modal").modal("hide");
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    table.ajax.reload(null, false);
                                    swal("Data pinjaman berhasil disetujui!", {
                                        icon: "success",
                                    }).then(function () {
                                        table.ajax.reload(null, false);
                                        //window.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: title,
                                        text: content,
                                        type: tag
                                    });
                                }
                            }
                        });
                    }
                });
            });

            /*REJECT LOAN*/
            $('#loan_table').on("click", ".btn-reject", function () {
                var loan_id = $(this).attr('x-data').replace(/\'/g, "\"");
                swal({
                    title: "Apakah Anda yakin ingin menolak data pinjaman ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willReject) => {
                    if (willReject) {
                        $.ajax({
                            type: 'POST',
                            url: "{!! route('aksi.loans') !!}",
                            dataType: 'json',
                            data: {
                                loan_id: loan_id,
                                aksi: 'reject'
                            },
                            success: function (res) {
                                //$("#loans_modal").modal("hide");
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    table.ajax.reload(null, false);
                                    swal("Data pinjaman berhasil ditolak!", {
                                        icon: "success",
                                    }).then(function () {
                                        table.ajax.reload(null, false);
                                        //window.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: title,
                                        text: content,
                                        type: tag
                                    });
                                }
                            }
                        });
                    }
                });
            });

            /*PAID LOAN*/
            $('#loan_table').on("click", ".btn-paid", function () {
                var loan_id = $(this).attr('x-data').replace(/\'/g, "\"");
                swal({
                    title: "Apakah pinjaman ini sudah lunas?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willPaid) => {
                    if (willPaid) {
                        $.ajax({
                            type: 'POST',
                            url: "{!! route('aksi.loans') !!}",
                            dataType: 'json',
                            data: {
                                loan_id: loan_id,
                                aksi: 'paid'
                            },
                            success: function (res) {
                                //$("#loans_modal").modal("hide");
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    table.ajax.reload(null, false);
                                    swal("Data pinjaman LUNAS!", {
                                        icon: "success",
                                    }).then(function () {
                                        table.ajax.reload(null, false);
                                        //window.location.reload();
                                    });
                                } else {
                                    swal({
                                        title: title,
                                        text: content,
                                        type: tag
                                    });
                                }
                            }
                        });
                    }
                });
            });


        });
    </script>
@endsection
