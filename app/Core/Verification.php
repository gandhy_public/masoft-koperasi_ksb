<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 1/25/18
 * Time: 4:59 AM
 */

namespace App\Core;


use App\Exceptions\VerificationException;
use Illuminate\Support\Facades\Input;

class Verification extends Core
{

    public static function check($key,$message=''){
      if(Input::get($key) ==null) {
          if ($message == ''){
              throw new VerificationException($key . " param required.");
          }else {
              throw new VerificationException(str_replace("%key%",ucfirst($key),$message));
          }
      }
      return Constants::STATUS_SUCCESS;
    }
}