<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('email')->unique();
            $table->string('name');
            $table->enum('role', ['Member', 'Admin', 'Super Admin']);
            $table->enum('gender', ['Pria', 'Wanita']);
            $table->string('phone');
            $table->string('address');
            $table->integer('postal_code');
            $table->string('picture')->default("no-avatar.png");
            $table->integer('balance')->default(0);
            $table->string('salt')->unique();
            $table->string('session_id');
            $table->enum('status', ['active', 'inactive', 'notapproved'])->default("active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
