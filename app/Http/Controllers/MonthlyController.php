<?php

namespace App\Http\Controllers;

use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use App\Models\Saving;
use App\Models\User;
use Illuminate\Http\Request;
use App\Core\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class MonthlyController extends Controller
{
    public function getMonthlyBonusInitTree()
    {
        try {
            Verification::check(Constants::USER_ID);
            $res = array();
            $res = DB::table(Constants::USERS)
            ->select(
                Constants::NAME,
                Constants::USER_ID,
                Constants::PICTURE,
                Constants::MEMBER_DETAILS.".".Constants::ID
                )
            ->join(Constants::MEMBER_DETAILS,Constants::SALT,"=",Constants::USER_ID)
            ->where(Constants::SALT,Input::get(Constants::USER_ID))
            ->first();
            $res->child = $this->getMonthlyChild(Input::get(Constants::USER_ID));
//            while($childCount>0){
//            $rsx = $res->child;
                foreach ($res->child as $c1){
                    $c1->child = $this->getMonthlyChild($c1->user_id);
                    foreach ($c1->child as $c2) {
                        $c2->child = $this->getMonthlyChild($c2->user_id);
                        foreach ($c2->child as $c3) {
                            $c3->child = $this->getMonthlyChild($c3->user_id);
                            foreach ($c3->child as $c4) {
                                $c4->child = $this->getMonthlyChild($c4->user_id);
                            }
                        }
                    }
//                    $rsx = $c->child;
                }
//                $childCount--;
//            }
            return Response::success($res);
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getMonthlyBonusTree()
    {
        try {
            Verification::check(Constants::USER_ID);
            $res = array();
            $res = DB::table(Constants::USERS)
            ->select(
                Constants::NAME,
                Constants::USER_ID,
                Constants::PICTURE,
                Constants::MEMBER_DETAILS.".".Constants::ID
                )
            ->join(Constants::MEMBER_DETAILS,Constants::SALT,"=",Constants::USER_ID)
            ->where(Constants::SALT,Input::get(Constants::USER_ID))
            ->first();
            $res->child = $this->getMonthlyChild(Input::get(Constants::USER_ID));
            return Response::success($res);
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (\Exception $e){
            return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }




    private function getMonthlyChild($user_id){
        return DB::table(Constants::MONTHLY)
            ->select(
                Constants::USERS.".".Constants::NAME,
                Constants::REFERENCES.".".Constants::USER_ID,
                Constants::PICTURE,
                Constants::MEMBER_DETAILS.".".Constants::ID,
                Constants::REFERENCES.".".Constants::REF_NO
            )
            ->join(Constants::REFERENCES,Constants::REFERENCES.".".Constants::ID,"=",Constants::MONTHLY.".".Constants::REF_ID)
            ->join(Constants::MEMBER_DETAILS,Constants::REFERENCES.".".Constants::USER_ID,"=",Constants::MEMBER_DETAILS.".".Constants::USER_ID)
            ->join(Constants::USERS,Constants::REFERENCES.".".Constants::USER_ID,"=",Constants::USERS.".".Constants::SALT)
            ->orderBy(Constants::REFERENCES.".".Constants::REF_NO,"ASC")
            ->where(Constants::PARENT_ID,$user_id)->get();
    }

}
