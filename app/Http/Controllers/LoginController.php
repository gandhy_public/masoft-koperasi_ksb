<?php

namespace App\Http\Controllers;

use App\Core\Constants;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index()
    {
        session_start();
        if (!isset($_SESSION[Constants::SESSION_USER_ID])) {
            return view('login');
        } else {
            return redirect()->route('home');
        }
    }
}
