@extends('templates.index')
@section('title')
    Simpanan Wajib dan Pokok
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Simpanan Wajib dan Pokok</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Simpanan</a>
            </li>
            <li class="active">Wajib & Pokok</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> Tabel Simpanan Wajib dan Pokok</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                <input type="radio" name="options" class="toggle" id="option1">Tambah</label>
                            {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">--}}
                                {{--<input type="radio" name="options" class="toggle" id="option2">Settings</label>--}}
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                        <tr>
                            <th> Nomor Anggota </th>
                            <th> Nama Anggota </th>
                            <th> Tanggal  </th>
                            <th> Jumlah </th>
                            <th> Tipe </th>
                            <th> Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="odd gradeX">
                            <td> shuxer </td>
                            <td>
                                <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                            </td>
                            <td> 120 </td>
                            <td class="center"> 12 Jan 2012 </td>
                            <td>
                                <span class="label label-sm label-success"> Approved </span>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                            <td> looper </td>
                            <td>
                                <a href="mailto:looper90@gmail.com"> looper90@gmail.com </a>
                            </td>
                            <td> 120 </td>
                            <td class="center"> 12.12.2011 </td>
                            <td>
                                <span class="label label-sm label-warning"> Suspended </span>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                            <td> userwow </td>
                            <td>
                                <a href="mailto:userwow@yahoo.com"> userwow@yahoo.com </a>
                            </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <span class="label label-sm label-success"> Approved </span>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                            <td> user1wow </td>
                            <td>
                                <a href="mailto:userwow@gmail.com"> userwow@gmail.com </a>
                            </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <span class="label label-sm label-default"> Blocked </span>
                            </td>
                        </tr>
                        <tr class="odd gradeX">
                            <td> restest </td>
                            <td>
                                <a href="mailto:userwow@gmail.com"> test@gmail.com </a>
                            </td>
                            <td> 20 </td>
                            <td class="center"> 12.12.2012 </td>
                            <td>
                                <span class="label label-sm label-success"> Approved </span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="modal fade bs-modal-lg" id="register" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Pendaftaran Anggota</h4>
                </div>
                <form id="form-register" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Anggota">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Username</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="username" placeholder="Username Anggota">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" name="email" placeholder="Email Anggota">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nomor Telepon</label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" name="phone" placeholder="Nomor Telepon Anggota">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alamat</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="address" placeholder="Alamat Anggota">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Password</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="password" placeholder="Password Anggota">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Konfirmasi Password</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="confirm_password" placeholder="Konfirmasi Password Anggota">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                        <button type="button" class="btn green" onclick="add()">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Detail Anggota</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="profile-userpic">
                                <img src="{{ asset('assets/pages/media/profile/profile_user.jpg') }}" class="img-responsive" alt="">
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name" id="name"> name</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <form class="form-horizontal" id="form-detail">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama</label>
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="user_id" id="field_id">
                                            <input type="text" class="form-control" name="name" id="field_name" placeholder="Nama Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Username</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="username" id="field_username" placeholder="Username Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" class="form-control" name="email" id="field_email" placeholder="Email Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor Telepon</label>
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="phone" id="field_phone" placeholder="Nomor Telepon Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="address" id="field_address" placeholder="Alamat Anggota">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-outline" id="delete" onclick="">Delete</button>
                    <button type="button" class="btn btn-success btn-outline" id="edit" onclick="save()">Simpan</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){
        var table=  $('#sample_1').DataTable( {
            dom: "Bfrtip",
            ajax: {
                "url":"{{ url("/api/v1/getTableMandatorySaving") }}",
                "type":"POST"
            },
            columns: [
                { data: "username" },
                { data: "name" },
                { data: "saving_time"},
                { data: "amount"},
                { data: "saving_type"},
                { sortable: false,
                    "render": function ( data, type, full, meta ) {
                        var res = "";
                        res +=  "<a href=\"#\" id=\"view_"+full.id+"\" class=\"btn-view fa fa-eye\" role=\"button\" x-data=\"" + JSON.stringify(full).replace(/\"/g,"'") + "\"></a> ";
                        res +=  "<a href=\"#\" id=\"edit_"+full.id+"\" class=\"btn-edit fa fa-pencil\" role=\"button\"  x-data=\"" + JSON.stringify(full).replace(/\"/g,"'") + "\"></a> ";
                        res +=  "<a href=\"#\" id=\"delete_"+full.id+"\" class=\"btn-delete fa fa-trash\" role=\"button\"  x-data=\"" + full.id + "\"></a>";
                        return res;
                    }}
            ],
            select: true,
            buttons:[]
        });
        });
    </script>
@endsection