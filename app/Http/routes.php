<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@dashboard');
Route::get('/login', 'LoginController@index')->name('login');
Route::get('/home', 'HomeController@dashboard')->name('home');
Route::get('/anggota', 'AnggotaController@dataAnggota')->name('anggota');
Route::get('/logout', 'UserController@logout')->name('logout');


/* Users */
Route::post('/api/v1/registerMember', function(){ return \App\Core\Auth::check('UserController@addMember',"controller",'Admin');})->name('post.register.member');
Route::post('/api/v1/uploadUserPicture', function(){ return \App\Core\Auth::check('UserController@uploadUserPicture',"controller",'Admin');});
Route::post('/api/v1/login', "UserController@login")->name('post.login');
Route::post('/api/v1/getUserDetails', 'UserController@getUserDetails')->name('post.detailuser');
Route::post('/api/v1/deleteUser', 'UserController@deleteUser')->name('post.delete');
Route::post('/api/v1/saveUser', 'UserController@edit')->name('post.save');

/* Referal */
Route::post('/api/v1/getUserReferalOption', function(){ return \App\Core\Auth::check('ReferenceController@getUserReferalOption',"controller",'Admin');})->name('get.user.referal.option');

//Route::get('/welcome', function(){ return \App\Core\Auth::check('welcome',"view",'suadmin');});


/* Simpanan */
//--API
Route::post('/api/v1/getTableMandatorySaving', function () {
    return \App\Core\Auth::check('SavingController@getTableMandatorySaving', "controller", 'Admin');
});
Route::post('/api/v1/getMemberSaving', function () {
    return \App\Core\Auth::check('SavingSukarelaController@getMembers', "controller", 'Admin');
})->name('get.member.saving');
Route::post('/api/v1/addSavingSukarela', function () {
    return \App\Core\Auth::check('SavingSukarelaController@addSavingSukarela', "controller", 'Admin');
})->name('add.saving.sukarela');
Route::post('/api/v1/getSavingSukarela', function () {
    return \App\Core\Auth::check('SavingSukarelaController@getSavingSukarela', "controller", 'Admin');
})->name('get.byid.saving.sukarela');
//--PAGES
Route::get('/simpanan/wajibpokok', function(){ return \App\Core\Auth::check('page/simpanan/wajibpokok',"view",'Admin');});
Route::get('/simpanan/sukarela', function () {
    return \App\Core\Auth::check('page/simpanan/sukarela', "view", 'Admin');
});


/* Pinjaman */
//--API
Route::post('/api/v1/getLoans', function () {
    return \App\Core\Auth::check('LoanController@getLoans', "controller", 'All');
})->name("get.loans");
Route::post('/api/v1/addLoans', function () {
    return \App\Core\Auth::check('LoanController@addLoans', "controller", 'Member');
})->name("add.loans");
Route::post('/api/v1/aksiLoans', function () {
    return \App\Core\Auth::check('LoanController@aksiLoans', "controller", 'All');
})->name("aksi.loans");

//--PAGES
Route::get('/pinjaman', function () {
    return \App\Core\Auth::check('page/pinjaman/pinjaman', "view", 'All');
});


/* Sponsorship */
//--API
Route::post('/api/v1/getSponsorshipBonusTree', function(){ return \App\Core\Auth::check('SponsorshipController@getSponsorshipBonusTree',"controller",'All'); });
//--PAGES
Route::get('/bonus/perekrutan', function(){ return \App\Core\Auth::check('page/bonus/perekrutan',"view",'All');});

/* Monthly */
//--API
Route::post('/api/v1/getMonthlyBonusTree', function(){ return \App\Core\Auth::check('MonthlyController@getMonthlyBonusTree',"controller",'All'); });
//--PAGES
Route::get('/bonus/bulanan', function(){ return \App\Core\Auth::check('page/bonus/bulanan',"view",'All');});


/* Branch */
//--API
Route::post('/api/v1/addBranch', function(){ return \App\Core\Auth::check('BranchController@addBranch',"controller",'Super Admin'); });

Route::post('/api/v1/getTableBranch', function(){ return \App\Core\Auth::check('BranchController@getTableBranch',"controller",'Super Admin'); });
Route::post('/api/v1/getBranchDetails', function(){ return \App\Core\Auth::check('BranchController@getBranchDetails',"controller",'Super Admin'); });

Route::post('/api/v1/getBranchOption', function(){ return \App\Core\Auth::check('BranchController@getBranchOption',"controller",'All'); })->name("get.branch.option");

Route::post('/api/v1/deleteBranch', function(){ return \App\Core\Auth::check('BranchController@deleteBranch',"controller",'Super Admin'); });
Route::post('/api/v1/updateBranch', function () {
    return \App\Core\Auth::check('BranchController@updateBranch', "controller", 'Super Admin');
});



Route::post('/api/v1/uploadBranchLogo', function(){ return \App\Core\Auth::check('BranchController@uploadBranchLogo',"controller",'Super Admin'); });
Route::post('/api/v1/getBranchBank', function(){ return \App\Core\Auth::check('BranchController@getBranchBank',"controller",'All'); });
//--PAGES
Route::get('/setting/branch', function(){ return \App\Core\Auth::check('page/branch/branch',"view",'Super Admin');});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

