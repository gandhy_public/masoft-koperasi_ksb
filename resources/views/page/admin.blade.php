@extends('templates.index')
@section('content')
    <div class="breadcrumbs">
        <h1>Anggota</h1><h4>Halaman Data Admin</h4>
        <div class="pull-right">
            <div class="input-icon right">
                <i class="icon-magnifier"></i>
                <input type="text" oninput="searching()" class="form-control form-control-solid" placeholder="search...">
            </div>
        </div>
        <button class="btn btn-info center" data-toggle="modal" href="#register"><i class="fa fa-plus-square"></i> Tambah Admin</button>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        @foreach($data as $key)
            <div class="col-md-2" onclick="detail('{{ $key->salt }}')">
                <div class="profile-userpic">
                    <img src="{{ asset('assets/pages/media/profile/profile_user.jpg') }}" class="img-responsive" alt=""> </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> {{ $key->name }} </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="modal fade bs-modal-lg" id="register" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Tambah Admin</h4>
                </div>
                <form id="form-register" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name" placeholder="Nama Anggota">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Username</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="username" placeholder="Username Anggota">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="email" placeholder="Email Anggota">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor Telepon</label>
                        <div class="col-md-9">
                            <input type="number" class="form-control" name="phone" placeholder="Nomor Telepon Anggota">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Alamat</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="address" placeholder="Alamat Anggota">
                        </div>
                    </div>
                        <hr>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password" placeholder="Password Anggota">
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="col-md-3 control-label">Konfirmasi Password</label>
                         <div class="col-md-9">
                             <input type="password" class="form-control" name="confirm_password" placeholder="Konfirmasi Password Anggota">
                         </div>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="button" class="btn green" onclick="add()">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Detail Admin</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="profile-userpic">
                                    <img src="{{ asset('assets/pages/media/profile/profile_user.jpg') }}" class="img-responsive" alt="">
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name" id="name"> name</div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <form class="form-horizontal" id="form-detail">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama</label>
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="user_id" id="field_id">
                                            <input type="text" class="form-control" name="name" id="field_name" placeholder="Nama Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Username</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="username" id="field_username" placeholder="Username Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" class="form-control" name="email" id="field_email" placeholder="Email Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor Telepon</label>
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="phone" id="field_phone" placeholder="Nomor Telepon Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="address" id="field_address" placeholder="Alamat Anggota">
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-outline" id="delete" onclick="">Delete</button>
                        <button type="button" class="btn btn-success btn-outline" id="edit" onclick="save()">Simpan</button>
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script>
        function add() {
            $.ajax({
                url: '{{ route('post.admin') }}',
                data: $("#form-register").serialize(),
                type: 'post',
                dataType: 'json',
                success: function(r) {
                    if(r.status == 1) {
                        $("#notif").html(r.data);
                        $("#notif").show('slow');
                        $("#register").modal('hide');
                        $("#form-register").reset();
                    } else {
                        $("#notif").html("Error");
                        $("#notif").show('slow');
                        $("#register").modal('hide');
                    }
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            })
        }
        function detail(param) {
            $.ajax({
                url: '{{ route('post.detailuser') }}',
                data: { user_id: param},
                type: 'post',
                dataType: 'json',
                success: function(r) {
                    if(r.status == 1) {
                        $("#name").html(r.data.name);
                        $("#field_id").val(r.data.salt);
                        $("#field_name").val(r.data.name);
                        $("#field_username").val(r.data.username);
                        $("#field_email").val(r.data.email);
                        $("#field_phone").val(r.data.phone);
                        $("#field_address").val(r.data.address);
                        $("#delete").attr('onclick', "deletes('"+r.data.salt+"')");
                        $("#detail").modal('show');
                    } else {
                        $("#notif").html("Error");
                        $("#notif").show('slow');
                    }
                }
            })
        }
        function deletes(id) {
            $.ajax({
                url: '{{ route('post.delete') }}',
                data: {user_id: id},
                type: 'post',
                dataType: 'json',
                success: function (r) {
                    if (r.status == 1) {
                        $("#notif").html('Berhasil menghapus data admin.');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    } else {
                        $("#notif").html('Error');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    }
                }
            })
        }
        function save() {
            $.ajax({
                url: '{{ route('post.save') }}',
                data: $("#form-detail").serialize(),
                type: 'post',
                dataType: 'json',
                success: function (r) {
                    if (r.status == 1) {
                        $("#notif").html('Berhasil merubah data admin.');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    } else {
                        $("#notif").html('Error');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    }
                }
            })
        }
        function searching() {
            $.ajax({

            })
        }
    </script>
@endsection