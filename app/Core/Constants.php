<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 1/23/18
 * Time: 8:37 PM
 */

namespace App\Core;


class Constants
{
    /* General */
    const STATUS = "status";
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 0;
    const MESSAGE = "message";
    const DATA = "data";
    const SERVER_FAILURE = "Server failure, please try again later...";

    /* General Field */
    const USERNAME = "username";
    const PASSWORD = "password";
    const CREATED_AT = "created_at";
    const USER_ID = "user_id";
    const BRANCH_ID = "branch_id";
    const ID = "id";
    const CODE = "code";
    const NAME = "name";
    const EMAIL = "email";
    const CONFIRM_PASSWORD = "confirm_password";
    const PHONE = "phone";
    const ADDRESS = "address";
    const SALT = "salt";
    const ROLE = "role";
    const VALUE = "value";
    const TYPE = "type";
    const IDR = "idr";
    const MONEY = "money";
    const UPDATED_AT = "updated_at";
    const UPDATED_BY = "updated_by";
    const OLD_PASSWORD = "old_password";
    const NEW_PASSWORD = "new_password";
    const DESCRIPTION = "description";
    const PICTURE = "picture";
    const LOGO = "logo";
    const POSTAL_CODE = "postal_code";
    const SESSION_ID = "session_id";
    const SUBJECT = "subject";
    const TO = "to";
    const BY = "by";
    const CONTENT = "content";
    const RECEIVER = "receiver";
    const POS = "pos";

    /* Session */
    const SESSION_USER_ID = "ksbid";
    const SESSION_ROLE = "ksbrole";

    /* Errors */
    const ERROR_NO_BANKS_DATA = "No data banks";
    const ERROR_NO_BRANCH_DATA = "No data branch";
    const ERROR_NO_USERS_DATA = "No users data";
    const ERROR_USER_NOT_FOUND = "User tidak ditemukan.";
    const ERROR_PASSWORD_NOT_VALID = "Password tidak cocok.";
    const ERROR_FAILED_REGISTER = "Gagal melakukan registrasi anggota.";

    /* Status */
    const STATUS_REDEEMED = "redeemed";
    const STATUS_EXPIRED = "expired";
    const STATUS_INACTIVE = "inactive";
    const STATUS_SUCCESS_STRING = "success";
    const STATUS_FAILED_STRING = "failed";
    const STATUS_ACTIVE = "active";
    const STATUS_REQUEST = "request";
    const STATUS_APPROVED = "approved";
    const STATUS_DISAPPROVED = "disapproved";
    const STATUS_READ = "read";
    const STATUS_DELETED = "deleted";


    /* Bank*/
    const BANK_ID = "bank_id";
    const BANK_NAME = "bank_name";
    const BANK_NUMBER = "bank_number";
    const ACCOUNT_NUMBER = "account_number";
    const ACCOUNT_NAME = "account_name";
    const ACTIVATION_CODE = "activation_code";
    const NO_ACTIVATION_DATA = "No activation data";
    const ERROR_NO_HISTORY_DATA = "No history data";

    /* Message */
    const MESSAGE_SUCCESS_EDIT_PROFILE = "Anda berhasil menyunting profil anda.";
    const MESSAGE_FAILED_PASSWORD_NOT_MATCH = "Password anda tidak cocok, Mohon ulangi.";
    const MESSAGE_SUCCESS_STATUS_SET = "Successfully set status";
    const MESSAGE_SUCCESS_REGISTER = "Berhasil mendaftarkan satu anggota.";

    const SAVINGS = "savings";
    const USERS = "users";
    const POKOK = "pokok";
    const WAJIB = "wajib";
    const BRANCH = "branches";
    const BANK_ACCOUNT = "bank_account";
    const BANK = "banks";
    const PARENT_ID = "parent_id";
    const INDEX_NUM = "index_num";
    const REF_ID = "ref_id";
    const REF_CODE = "ref_code";
    const REF_NO = "ref_no";
    const MEMBER_DETAILS = "member_details";
    const MEMBER_NUM = "member_num";
    const REF_QUOTA = "ref_quota";
    const BIRTH_PLACE = "birth_place";
    const BIRTH_DATE = "birth_date";
    const IDENTITY_TYPE = "identity_type";
    const IDENTITY_NUM = "identity_num";
    const MOTHER_NAME = "mother_name";
    const FIRST_HEIR = "first_heir";
    const SECOND_HEIR = "second_heir";
    const JOB_NAME = "job_name";
    const USER_REFERAL = "user_referal";
    const REFERENCES = "references";
//    const SPONSORSHIP = "sponsorship";
    const SPONSORSHIPS = "sponsorships";
    const MONTHLY = "monthly";

    // disable constructor
    private function __construct(){}

}