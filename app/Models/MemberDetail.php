<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'approved_by',
        'ref_quota',
        'ref_code',
        'birth_place',
        'birth_date',
        'identity_num',
        'identity_type',
        'mother_name',
        'first_heir',
        'second_heir',
        'job_name',
        'type',
        'branch_id',
        'created_at',
        'updated_at'
    ];
}
