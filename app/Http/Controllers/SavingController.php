<?php

namespace App\Http\Controllers;

use App\Core\Constants;
use App\Core\Response;
use App\Exceptions\VerificationException;
use Illuminate\Support\Facades\DB;

class SavingController extends Controller
{

    public function getTableMandatorySaving()
    {
        try {
            $data = DB::table(Constants::SAVINGS)
                ->select(
                    "savings.id as id",
                    "amount",
                    "savings.status as saving_status",
                    "savings.type as saving_type",
                    "savings.created_at as saving_time",
                    "username",
                    "name")
                ->join(Constants::USERS, Constants::USER_ID, "=", Constants::SALT)
                ->where(Constants::TYPE, Constants::POKOK)
                ->orWhere(Constants::TYPE, Constants::WAJIB)
                ->get();
            if (null == $data) throw new VerificationException("Tidak ada simpanan tercatat");
            return Response::table($data);
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (\Exception $e){
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

}
