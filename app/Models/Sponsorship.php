<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sponsorship extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sponsorships';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'parent_id',
        'ref_id',
        'status',
        'index_num',
        'created_at',
        'updated_at'
    ];
}
