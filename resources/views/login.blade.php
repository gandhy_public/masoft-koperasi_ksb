<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Login | Koperasi Keluarga Sejahtera Bersama</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/css/components-rounded.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/pages/css/login-2.min.css') }}" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="favicon.ico" /> </head>

    <body class=" login">
        <div class="logo">
            <h1 style="color:white;">KOPERASI KELUARGA SEJAHTERA BERSAMA</h1>
                {{--<img src="{{ asset('assets/pages/img/logo-big-white.png') }}" style="height: 17px;" alt="" />--}}
        </div>
        <div class="content">
            <form id="form-login">
                <div class="form-title">
                    <span class="form-title">Selamat Datang.</span>
                    <span class="form-subtitle">Masukkan Identitas Anda.</span>
                </div>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Masukkan identitas untuk login.</span>
                </div>
                <div class="alert alert-warning" style="display: none;" id="alert">
                    <button class="close" data-close="alert"></button>
                    <span> Username and password tidak ditemukan.</span>
                </div>
                <div class="alert alert-success" style="display: none;" id="success">
                    <button class="close" data-close="alert"></button>
                    <span> Login berhasil, tunggu redirect.</span>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="button" onclick="login()" class="btn red btn-block uppercase">Login</button>
                </div>
            </form>
        </div>
        <div class="copyright"> 2018 © Koperasi Keluarga Sejahtera Bersama </div>
        <script>
            function login() {
                $("#alert").hide('slow');
                $("#success").hide('slow');
                $.ajax({
                    url: '{{ route('post.login') }}',
                    data: $("#form-login").serialize(),
                    type: 'post',
                    dataType: 'json',
                    success: function(r) {
                        if(r.status == 1) {
                            $("#success").show('slow');
                            setInterval(function(){
                                window.location.replace('{{ route('home') }}');
                            }, 2000);
                        } else {
                            $("#alert").show('slow');
                        }
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    }
                })
            }
        </script>
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/pages/scripts/login.min.js') }}" type="text/javascript"></script>

    </body>
</html>