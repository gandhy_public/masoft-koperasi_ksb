<?php
/*DEVELOPING BY Gandhy Bagoes Cahyono*/
namespace App\Http\Controllers;

use App\Core\Constants;
use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use App\Models\BankAccount;
use App\Models\Branch;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class BranchController extends Controller
{


    public function getTableBranch()
    {
        try {
            $data = DB::table(Constants::BRANCH)
                ->select("id","name","postal_code","status","code","created_at")
                ->where(Constants::STATUS, "!=", Constants::STATUS_DELETED)
                ->get();
            if (null == $data) throw new VerificationException("Tidak ada branch");
            return Response::table($data);
        } catch(VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getBranchDetails()
    {
        try {
            Verification::check(Constants::BRANCH_ID, "%key% tidak boleh kosong.");
            $branch = DB::table(Constants::BRANCH)
                ->select(
                    "branches.code as branch_code",
                    "branches.name as branch_name",
                    "branches.address as branch_address",
                    "branches.postal_code as branch_postal",
                    "branches.phone as branch_phone",
                    "banks.short_name as branch_bank",
                    "banks.id as branch_bank_id",
                    "bank_account.account_name as branch_bank_name",
                    "bank_account.account_number as branch_bank_rekening",
                    "branches.status as branch_status",
                    "branches.logo as branch_logo",
                    "branches.created_at as branch_create")
                ->join(Constants::BANK_ACCOUNT, Constants::BRANCH.".".Constants::ID, "=", Constants::BANK_ACCOUNT.".".Constants::USER_ID)
                ->join(Constants::BANK, Constants::BANK_ACCOUNT.".".Constants::BANK_ID, "=", Constants::BANK.".".Constants::ID)
                ->where(Constants::BRANCH.".".Constants::STATUS, "!=", Constants::STATUS_DELETED)
                ->where(Constants::BRANCH.".".Constants::ID, Input::get(Constants::BRANCH_ID));
            //->get();
            if($branch->count() == 0) throw new VerificationException(Constants::ERROR_NO_BRANCH_DATA);
            return Response::success($branch->first());
        } catch (VerificationException $le) {
            return Response::failed($le->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function deleteBranch()
    {
        try {
            Verification::check(Constants::BRANCH_ID, "%key% tidak boleh kosong.");
            $branch = DB::table(Constants::BRANCH)
                ->where(Constants::STATUS, "!=", Constants::STATUS_DELETED)
                ->where(Constants::ID, Input::get(Constants::BRANCH_ID));
            if($branch->count() == 0 || null == $branch) throw new VerificationException(Constants::ERROR_NO_BRANCH_DATA);
            $branch->update([
                Constants::STATUS=>Constants::STATUS_DELETED,
                Constants::UPDATED_AT=>date("Y-m-d H:i:s"),
            ]);
            return Response::success("Hapus data sukses!");
        } catch (VerificationException $le) {
            return Response::failed($le->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function uploadBranchLogo()
    {
        try {
            $fileName = rand() . "_branch_logo";
            $fileType = explode('.', $_FILES["file"]["name"]);
            $fileType = $fileType[count($fileType)-1];
            $fileError = $_FILES['file']['error'];
            $fileContent = file_get_contents($_FILES['file']['tmp_name']);
            if(!in_array($fileType, array("jpg","jpeg","png","PNG"))) throw new VerificationException("File harus berformat gambar!");
            if($fileError == UPLOAD_ERR_OK){
                file_put_contents(public_path() . "/uploads/branches/" . $fileName . "." . $fileType, $fileContent);
                return  Response::success(array("message"=>"File berhasil di upload!","file_name"=>$fileName.".".$fileType));
            }else{
                switch($fileError){
                    case UPLOAD_ERR_INI_SIZE:
                        throw new VerificationException('Error INI FILE SIZE');
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new VerificationException('Error UPLOAD_ERR_FORM_SIZE');
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        throw new VerificationException('Error: UPLOAD_ERR_PARTIAL');
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new VerificationException('Error: UPLOAD_ERR_NO_FILE');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        throw new VerificationException('Error: UPLOAD_ERR_NO_TMP_DIR');
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        throw new VerificationException('Error: UPLOAD_ERR_CANT_WRITE');
                        break;
                    case  UPLOAD_ERR_EXTENSION:
                        throw new VerificationException('Error: UPLOAD_ERR_EXTENSION');
                        break;
                    default: throw new VerificationException('Error: Unkown file error.');
                        break;
                }
            }
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (QueryException $qe){
            return Response::failed("Tidak bisa import data, silahkan cek kembali data Anda");
        }catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getBranchBank()
    {
        try {
            $bank = DB::table(Constants::BANK)
                ->select("id","short_name","code_bank","full_name")
                ->get();
            if (null == $bank) throw new VerificationException("Tidak ada data bank");
            return Response::success($bank);
        } catch(VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getBranchOption()
    {
        try {
            $branch = Branch::select(Constants::ID,Constants::CODE,Constants::NAME)->get();
            if (null == $branch) throw new VerificationException("Tidak ada data branch");
            return Response::success($branch);
        } catch(VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function addBranch()
    {
        try {
            Verification::check(Constants::NAME,"Nama harus diisi!");
            Verification::check(Constants::ADDRESS,"Alamat harus diisi!");
            Verification::check(Constants::POS,"Kode Pos harus diisi!");
            Verification::check(Constants::PHONE,"Nomor Telepon harus diisi!");
            Verification::check(Constants::CODE,"Kode Cabang harus diisi!");
            Verification::check(Constants::BANK_ID,"Bank harus diisi!");
            Verification::check(Constants::BANK_NAME,"Nama Rekening harus diisi!");
            Verification::check(Constants::BANK_NUMBER,"Nomor Rekening harus diisi!");
            Verification::check(Constants::LOGO,"Logo harus diisi!");

            if(Branch::where([Constants::CODE => Input::get("code")])->count() != 0) throw new VerificationException("Kode Cabang sudah digunakan, gunakan Kode Cabang yang lain!");

            Branch::create([
                Constants::NAME => Input::get(Constants::NAME),
                Constants::ADDRESS => Input::get(Constants::ADDRESS),
                Constants::POSTAL_CODE => Input::get(Constants::POS),
                Constants::PHONE => Input::get(Constants::PHONE),
                Constants::LOGO => Input::get(Constants::LOGO),
                Constants::STATUS => "active",
                Constants::CODE => Input::get(Constants::CODE)
            ]);

            $total_branch = DB::table(Constants::BRANCH)
                ->select("id")
                ->orderBy(Constants::ID,"desc");
            //->count();
            $total_branch = $total_branch->first();
            $total_branch = $total_branch->id;

            BankAccount::create([
                Constants::USER_ID => $total_branch,
                Constants::BANK_ID => Input::get(Constants::BANK_ID),
                Constants::TYPE => "koperasi",
                Constants::ACCOUNT_NUMBER => Input::get(Constants::BANK_NUMBER),
                Constants::ACCOUNT_NAME => Input::get(Constants::BANK_NAME)
            ]);
            return Response::success("Anda berhasil menambahkan data cabang!");
        } catch(VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function updateBranch()
    {
        try {
            Verification::check(Constants::NAME,"Nama harus diisi!");
            Verification::check(Constants::ADDRESS,"Alamat harus diisi!");
            Verification::check(Constants::POS,"Kode Pos harus diisi!");
            Verification::check(Constants::PHONE,"Nomor Telepon harus diisi!");
            Verification::check(Constants::CODE,"Kode Cabang harus diisi!");
            Verification::check(Constants::BANK_ID,"Bank harus diisi!");
            Verification::check(Constants::BANK_NAME,"Nama Rekening harus diisi!");
            Verification::check(Constants::BANK_NUMBER,"Nomor Rekening harus diisi!");

            if(Input::get("code") != Input::get("code_asli")) {
                if(Branch::where([Constants::CODE => Input::get("code"),Constants::STATUS => "deleted"])->count() == 0) throw new VerificationException("Kode Cabang sudah digunakan, gunakan Kode Cabang yang lain!");
            }

            if(Input::get("logo") == "" || null == Input::get("logo")) {
                //throw new VerificationException("Logo tidak di update");
                $branch = Branch::where(Constants::ID , Input::get(Constants::ID));
                if ($branch->count() == 0 || null == $branch) throw new VerificationException("Data cabang tidak ditemukan!");
                $branch->update([
                    Constants::NAME => Input::get(Constants::NAME),
                    Constants::ADDRESS => Input::get(Constants::ADDRESS),
                    Constants::POSTAL_CODE => Input::get(Constants::POS),
                    Constants::PHONE => Input::get(Constants::PHONE),
                    Constants::CODE => Input::get(Constants::CODE)
                ]);

                $bank = BankAccount::where(Constants::USER_ID , Input::get(Constants::ID));
                if ($bank->count() == 0 || null == $bank) throw new VerificationException("Akun Bank tidak ditemukan!");
                $bank->update([
                    Constants::BANK_ID => Input::get(Constants::BANK_ID),
                    Constants::ACCOUNT_NUMBER => Input::get(Constants::BANK_NUMBER),
                    Constants::ACCOUNT_NAME => Input::get(Constants::BANK_NAME)
                ]);
                return Response::success("Anda berhasil mengupdate data cabang!");
            } else {
                $branch = Branch::where(Constants::ID , Input::get(Constants::ID));
                if ($branch->count() == 0 || null == $branch) throw new VerificationException("Data cabang tidak ditemukan!");
                $branch->update([
                    Constants::NAME => Input::get(Constants::NAME),
                    Constants::ADDRESS => Input::get(Constants::ADDRESS),
                    Constants::POSTAL_CODE => Input::get(Constants::POS),
                    Constants::PHONE => Input::get(Constants::PHONE),
                    Constants::CODE => Input::get(Constants::CODE),
                    Constants::LOGO => Input::get(Constants::LOGO)
                ]);

                $bank = BankAccount::where(Constants::USER_ID , Input::get(Constants::ID));
                if ($bank->count() == 0 || null == $bank) throw new VerificationException("Akun Bank tidak ditemukan!");
                $bank->update([
                    Constants::BANK_ID => Input::get(Constants::BANK_ID),
                    Constants::ACCOUNT_NUMBER => Input::get(Constants::BANK_NUMBER),
                    Constants::ACCOUNT_NAME => Input::get(Constants::BANK_NAME)
                ]);
                return Response::success("Anda berhasil mengupdate data cabang!");
            }
        } catch(VerificationException $ve) {
            return Response::failed($ve->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }


}
