@extends('templates.index')
@section('css')
    <link href="/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Anggota</h1><h4>Halaman Data Anggota</h4>
        <div class="pull-right">
            <div class="input-icon right">
                <i class="icon-magnifier"></i>
                <input type="text" oninput="searching()" class="form-control form-control-solid" placeholder="search...">
            </div>
        </div>
        <button id="btn_add_member" class="btn btn-info center" data-toggle="modal" href="#register"><i class="fa fa-plus-square"></i> Tambah Anggota</button>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        @foreach($data as $key)
            <div class="col-md-2" onclick="detail('{{ $key->salt }}')">
                <div class="profile-userpic">
                    <img src="{{ asset('assets/pages/media/profile/profile_user.jpg') }}" class="img-responsive" alt=""> </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> {{ $key->name }} </div>
                    <div class="profile-usertitle-job"> {{ $key->user_id }}</div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="modal fade bs-modal-lg" id="register" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Pendaftaran Anggota</h4>
                </div>
                <form id="form-register" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Referal</label>
                            <div class="col-md-9">
                                <select id="add_user_referal" class="form-control select2" name="user_referal">
                                    <option value="---">Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Cabang</label>
                            <div class="col-md-9">
                                <select id="add_branch" class="form-control select2" name="branch_id">
                                    <option value="---">Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9">
                                <input type="text" id="add_user_name" class="form-control" name="name" placeholder="Nama Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Username</label>
                            <div class="col-md-9">
                                <input type="text" id="add_user_username" class="form-control" name="username" placeholder="Username Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="email" id="add_user_email" class="form-control" name="email" placeholder="Email Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Telepon</label>
                            <div class="col-md-9">
                                <input type="number" id="add_user_phone" class="form-control" name="phone" placeholder="Nomor Telepon Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Alamat</label>
                            <div class="col-md-9">
                                <input type="text" id="add_user_address" class="form-control" name="address" placeholder="Alamat Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gambar</label>
                            <div class="col-md-9">
                                <input type="button" id="btn_upload_picture" class="form-control btn btn-primary" name="logo" value="Upload Gambar">
                                <input type="hidden" id="add_picture" class="form-control" name="picture">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tempat Lahir</label>
                            <div class="col-md-9">
                                <input type="text" id="add_birth_place" class="form-control" name="birth_place" placeholder="Tempat Lahir">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal Lahir</label>
                            <div class="col-md-9">
                                <input id="add_birth_date" name="birth_date" class="form-control form-control-inline date-picker" size="16" type="text" placeholder="Tanggal Lahir" value="" />
                                {{--<input type="text" id="add_birth_date" class="form-control" name="birth_date" placeholder="Tanggal Lahir">--}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jenis Kartu Identitas</label>
                            <div class="col-md-9">
                                <select name="identity_type" class="form-control select2" id="add_identity_type">
                                    <option value="ktp">KTP</option>
                                    <option value="sim">SIM</option>
                                    <option value="passport">Paspor</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Kartu Identitas</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="add_identity_num" name="identity_num" placeholder="Nomor Kartu Identitas">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Ibu</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="add_mother_name" name="mother_name" placeholder="Nama Ibu">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Ahli Waris 1</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="add_first_heir" name="first_heir" placeholder="Ahli Waris Pertama">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Ahli Waris 2</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="add_second_heir" name="second_heir" placeholder="Ahli Waris Kedua">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pekerjaan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="add_job_name" name="job_name" placeholder="Pekerjaan">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Bank</label>
                            <div class="col-md-9">
                                <select id="add_bank" class="form-control select2" name="bank_id">
                                    <option value="---">Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Rekening Bank</label>
                            <div class="col-md-9">
                                <input type="text" id="add_bank_name" class="form-control" name="account_name" placeholder="Nama Pemilik Rekening Bank">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Rekening Bank</label>
                            <div class="col-md-9">
                                <input type="text" id="add_bank_number" class="form-control" name="account_number" placeholder="Nomor Rekening Bank">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" id="add_password" name="password" placeholder="Password Anggota">
                            </div>
                        </div>
                        <div class="form-group">
                             <label class="col-md-3 control-label">Konfirmasi Password</label>
                             <div class="col-md-9">
                                 <input type="password" class="form-control" id="add_confirm_password" name="confirm_password" placeholder="Konfirmasi Password Anggota">
                             </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="button" class="btn green" onclick="add()">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-lg" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Detail Anggota</h4>
                </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="profile-userpic">
                                    <img src="{{ asset('assets/pages/media/profile/profile_user.jpg') }}" class="img-responsive" alt="">
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name" id="name"> name</div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <form class="form-horizontal" id="form-detail">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama</label>
                                        <div class="col-md-9">
                                            <input type="hidden" class="form-control" name="user_id" id="field_id">
                                            <input type="text" class="form-control" name="name" id="field_name" placeholder="Nama Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Username</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="username" id="field_username" placeholder="Username Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" class="form-control" name="email" id="field_email" placeholder="Email Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor Telepon</label>
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="phone" id="field_phone" placeholder="Nomor Telepon Anggota">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="address" id="field_address" placeholder="Alamat Anggota">
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-outline" id="delete" onclick="">Delete</button>
                        <button type="button" class="btn btn-success btn-outline" id="edit" onclick="save()">Simpan</button>
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        <input id="import-input" type="file"  name="name" style="display: none;" />
    </div>
@endsection
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script>
        $(document).ready(function(){
            $('#add_birth_date').datepicker();
            $('#btn_add_member').click(function(){
                $.ajax({
                    url: '{{ route('get.user.referal.option') }}',
                    data: {},
                    type: 'post',
                    dataType: 'json',
                    success: function(res) {
                        users = "";
                        for(var i = 0;i<res.data.length;i++){
                            users += '<option value="' + res.data[i].user_id + ';' + res.data[i].ref_code + '">[ ' + res.data[i].ref_code  + ' - '+ res.data[i].member_num +' ] ' + res.data[i].name + '</option>'
                        }
                        $('#add_user_referal').html(users);
                        $('#add_user_referal').select2({
                            width:"100%"
                        });
                    },
                    error: function(xhr) {
                        alert("Server Error!")
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getBranchBank') !!}",
                    dataType: 'json',
                    success : function(res){
                        banks = "";
                        for(var i = 0;i<res.data.length;i++){
                            banks += '<option value="' + res.data[i].id + '">' + res.data[i].full_name + '</option>'
                        }
                        $('#add_bank').html(banks);
                    }
                });
                $.ajax({
                    url: '{{ route('get.branch.option') }}',
                    data: {},
                    type: 'post',
                    dataType: 'json',
                    success: function(res) {
                        users = "";
                        for(var i = 0;i<res.data.length;i++){
                            users += '<option value="' + res.data[i].id + '">[ ' + res.data[i].code  + ' ] ' + res.data[i].name + '</option>'
                        }
                        $('#add_branch').html(users);
                        $('#add_branch').select2({
                            width:"100%"
                        });
                    },
                    error: function(xhr) {
                        alert("Server Error!")
                    }
                });

            });

            $("#btn_upload_picture").click(function(){
                $('#import-input').trigger('click');
            });
            $("#import-input").change(function(){
                swal({
                    title: 'Importing!',
                    text: 'Please wait and don\'t refresh the page.',
                    timer: 4000,
                    showCancelButton: false,
                    showConfirmButton: false
                }).then(
                    function(){
                        var filex = $("#import-input")[0].files[0];
                        var fd = new FormData();
                        fd.append('file', filex);
                        $.ajax({
                            type: 'POST',
                            url: "{!! url('/api/v1/uploadUserPicture')!!}",
                            data: fd,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            success : function(res){
                                var title = "Error!";
                                var content = res.message;
                                var tag = "error";
                                if (res.status === 1) {
                                    title = "Imported!";
                                    content = res.data.message;
                                    tag = "success";
                                    $("#add_picture").attr("value",res.data.file_name);
                                }
                                swal({
                                    title:title,
                                    text:content,
                                    type:tag
                                })
                            }
                        });
                    },
                    function(dismiss){
                        if (dismiss === 'timer') {
                            //console.log('I was closed by the timer')
                        }
                    }
                )
            });
        });
        function add() {
            $.ajax({
                url: '{{ route('post.register.member') }}',
                data: $("#form-register").serialize(),
                type: 'post',
                dataType: 'json',
                success: function(res) {
                    var title = "Error!";
                    var content = res.message;
                    var tag = "error";
                    if (res.status === 1) {
                        title = "Ditambahkan!";
                        content = res.data.message;
                        tag = "success";
                        $("#register").modal('hide');
                        $("#form-register")[0].reset();
                    }
                    swal({
                        title:title,
                        text:content,
                        type:tag
                    });
                    // if(r.status == 1) {
                    //     $("#notif").html(r.data);
                    //     $("#notif").show('slow');
                    //     $("#register").modal('hide');
                    //     $("#form-register").reset();
                    // } else {
                    //     $("#notif").html("Error");
                    //     $("#notif").show('slow');
                    //     $("#register").modal('hide');
                    // }
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            })
        }
        function detail(param) {
            $.ajax({
                url: '{{ route('post.detailuser') }}',
                data: { user_id: param},
                type: 'post',
                dataType: 'json',
                success: function(r) {
                    if(r.status == 1) {
                        $("#name").html(r.data.name);
                        $("#field_id").val(r.data.salt);
                        $("#field_name").val(r.data.name);
                        $("#field_username").val(r.data.username);
                        $("#field_email").val(r.data.email);
                        $("#field_phone").val(r.data.phone);
                        $("#field_address").val(r.data.address);
                        $("#delete").attr('onclick', "deletes('"+r.data.salt+"')");
                        $("#detail").modal('show');
                    } else {
                        $("#notif").html("Error");
                        $("#notif").show('slow');
                    }
                }
            })
        }
        function deletes(id) {
            $.ajax({
                url: '{{ route('post.delete') }}',
                data: {user_id: id},
                type: 'post',
                dataType: 'json',
                success: function (r) {
                    if (r.status == 1) {
                        $("#notif").html('Berhasil menghapus data anggota.');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    } else {
                        $("#notif").html('Error');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    }
                }
            })
        }
        function save() {
            $.ajax({
                url: '{{ route('post.save') }}',
                data: $("#form-detail").serialize(),
                type: 'post',
                dataType: 'json',
                success: function (r) {
                    if (r.status == 1) {
                        $("#notif").html('Berhasil merubah data anggota.');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    } else {
                        $("#notif").html('Error');
                        $("#notif").show('fade in');
                        $("#detail").modal('hide');
                    }
                }
            })
        }
        function searching() {
            $.ajax({

            })
        }
    </script>
@endsection