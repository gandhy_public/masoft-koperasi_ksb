<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ isset($title)?$title:"" }} @yield('title') | Koperasi Keluarga Sejahtera Bersama</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        @include('templates.css')
        @yield('css')
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <div class="wrapper">

            @include('templates.menu')

            <div class="container-fluid">
                <div class="page-content">

                    @yield('content')

                    <div class="clearfix"></div>
                </div>

                <p class="copyright">2018 © Koperasi Keluarga Sejahtera Bersama.
                </p>
                <a href="#index" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>

            </div>
        </div>

        @include('templates.js')
        @yield('js')
    </body>
</html>