@extends('templates.index')
@section('title')
    Bonus Bulanan
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <style>
        /*Now the CSS*/

        .detail-label{
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin-bottom: 10px !important;
            margin-top: 0 !important;
        }


        .tree ul {
            padding-top: 20px; position: relative;
            width: 100%;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        .tree img{
            max-width: 120px!important;
            border-radius: 100%;
        }

        .tree li {
            float: left; text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*We will use ::before and ::after to draw the connectors*/

        .tree li::before, .tree li::after{
            content: '';
            position: absolute; top: 0; right: 50%;
            border-top: 1px solid #ccc;
            width: 50%; height: 20px;
        }
        .tree li::after{
            right: auto; left: 50%;
            border-left: 1px solid #ccc;
        }

        /*We need to remove left-right connectors from elements without
        any siblings*/
        .tree li:only-child::after, .tree li:only-child::before {
            display: none;
        }

        /*Remove space from the top of single children*/
        .tree li:only-child{ padding-top: 0;}

        /*Remove left connector from first child and
        right connector from last child*/
        .tree li:first-child::before, .tree li:last-child::after{
            border: 0 none;
        }
        /*Adding back the vertical connector to the last nodes*/
        .tree li:last-child::before{
            border-right: 1px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
        }
        .tree li:first-child::after{
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
        }

        /*Time to add downward connectors from parents*/
        .tree ul ul::before{
            content: '';
            position: absolute; top: 0; left: 50%;
            border-left: 1px solid #ccc;
            width: 0; height: 20px;
        }


        .tree li a{
            border: 1px solid #ccc;
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;

            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
        }

        /*Time for some hover effects*/
        /*We will apply the hover effect the the lineage of the element also*/
        .tree li a:hover, .tree li a:hover+ul li a {
            background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
        }
        /*Connector styles on hover*/
        .tree li a:hover+ul li::after,
        .tree li a:hover+ul li::before,
        .tree li a:hover+ul::before,
        .tree li a:hover+ul ul::before{
            border-color:  #94a0b4;
        }

        /*Thats all. I hope you enjoyed it.
        Thanks :)*/
        .select2{
            z-index: 98!important;
        }
    </style>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Bonus Bulanan</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Bonus</a>
            </li>
            <li class="active">Bulanan</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered" id="child-container">
                <div class="portlet-title">
                        <div class="caption">
                                <span  class="caption-subject font-green-sharp bold uppercase">Diagram Bonus Bulanan</span>
                        </div>
                </div>
                <div class="portlet-body">
                    <h4 class="block"></h4>
                    <div class="row">
                        <?php if($_SESSION['ksbrole'] == 'Admin' || $_SESSION['ksbrole'] == 'Super Admin'){?>
                        <div class="col-sm-12 col-md-offset-4 col-md-4">
                            <center>Pilih Anggota : </center>
                            <br>
                            <select id="select_user" class="form-control select2">
                                <option value="---">Loading...</option>
                            </select>
                            <br>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <center>
                                <div id="myDiagramDiv"
                                     style="width:100%; height:500px; background-color: #DAE4E4;"></div>
                            </center>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>


@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gojs/1.8.16/go-debug.js"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        Array.prototype.removeValue = function(name, value){
            var array = $.map(this, function(v,i){
                return v[name] === value ? null : v;
            });
            this.length = 0; //clear original array
            this.push.apply(this, array); //push all elements except the one we want to delete
        }
        $(document).ready(function(){
            var $g = go.GraphObject.make;
            var dataArr=[];
            var myDiagram =
                $g(go.Diagram, "myDiagramDiv",
                    {
                        initialContentAlignment: go.Spot.Center, // center Diagram contents
                        "undoManager.isEnabled": true, // enable Ctrl-Z to undo and Ctrl-Y to redo
                        layout: $g(go.TreeLayout, // specify a Diagram.layout that arranges trees
                            { angle: 90, layerSpacing: 35 })
                    });

// the template we defined earlier
            myDiagram.nodeTemplate =
                $g(go.Node, "Vertical",
                    { background: "#44CCFF" },
                    $g(go.Picture,
                        {
                            margin: 5,
                            width: 80,
                            height: 80,
                            background: "red"
                        },
                        new go.Binding("source")),
                    $g(go.TextBlock,
                        {
                            text: "Loading...",
                            background: "#44CCFF", margin: 5,
                            isMultiline: true,
                            textAlign: "center"
                        }
                        ,new go.Binding("text"))
                );

// define a Link template that routes orthogonally, with no arrowhead
            myDiagram.linkTemplate =
                $g(go.Link,
                    { routing: go.Link.Orthogonal, corner: 5 },
                    $g(go.Shape, { strokeWidth: 3, stroke: "#555" })); // the link shape

            myDiagram.addDiagramListener("ObjectSingleClicked", function (ev) {
                if(!ev.subject.part.data.clicked ) {
                    if (ev.subject.part.data.level < 12) {
                        $.ajax({
                            type: 'POST',
                            url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                            dataType: 'json',
                            data: {
                                user_id: ev.subject.part.data.key
                            },
                            success: function (res) {
                                try {
                                    appendMonthly(res.data, (ev.subject.part.data.level + 1), false);
                                } catch (err) {
                                    console.log(err);
                                }
                            }
                        });
                    }
                    <?php if($_SESSION['ksbrole'] == 'Admin' || $_SESSION['ksbrole'] == 'Super Admin'){?>
                    else{
                     changeParent(ev.subject.part.data.key);
                    }
                    <?php }else{ ?>
                    else{
                        swal("Maaf","Anda hanya diperbolehkan melihat 12 level dibawah anda","warning");
                    }
                    <?php } ?>
                }
                ev.subject.part.data.clicked = true;
            });


            var model = $g(go.TreeModel);
            model.nodeDataArray =
                [
                    { key: "1",              text: "Don Meow\n123",   source: "/uploads/users/no-avatar.png" },
                    { key: "as", parent: "1", text: "Demeter",    source: "/uploads/users/no-avatar.png" },
                    { key: "3", parent: "1", text: "Copricat",   source: "/uploads/users/no-avatar.png" },
                    { key: "4", parent: "3", text: "Jellylorum", source: "/uploads/users/no-avatar.png" },
                    { key: "5", parent: "3", text: "Alonzo",     source: "/uploads/users/no-avatar.png" },
                    { key: "6", parent: "as", text: "Munkustrap", source: "/uploads/users/no-avatar.png" }
                ];
            myDiagram.model = model;

            {{--$.ajax({--}}
                {{--type: 'POST',--}}
                {{--url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",--}}
                {{--dataType: 'json',--}}
                {{--data:{--}}
                    {{--user_id:"$49213jfdsajkk.4KSKJTestSalt1"--}}
                {{--},--}}
                {{--success : function(res){--}}
                    {{--try{--}}
                        {{--appendMonthly(res.data,(1));--}}
                    {{--}catch (err){--}}
                        {{--console.log(err);--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}

            <?php if($_SESSION['ksbrole'] == 'Admin' || $_SESSION['ksbrole'] == 'Super Admin'){?>
            $.ajax({
                url: '{{ route('get.user.referal.option') }}',
                data: {},
                type: 'post',
                dataType: 'json',
                success: function(res) {
                    users = "";
                    for(var i = 0;i<res.data.length;i++){
                        users += '<option value="' + res.data[i].user_id + '">[ ' + res.data[i].ref_code  + ' - '+ res.data[i].member_num +' ] ' + res.data[i].name + '</option>'
                    }
                    $('#select_user').html(users);
                    $('#select_user').select2({
                        width:"100%"
                    });
                    changeParent($('#select_user').val());
                },
                error: function(xhr) {
                    alert("Server Error!")
                }
            });
            $('#select_user').change(function () {
                dataArr = [];
                changeParent($('#select_user').val());
            });
            function changeParent(user_id) {
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                    dataType: 'json',
                    data:{
                        user_id:user_id
                    },
                    success : function(res){
                        try{
                            appendMonthly(res.data,1,true);
                            for(var i = 0;i<res.data.child.length;i++)
                            $.ajax({
                                type: 'POST',
                                url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                dataType: 'json',
                                data:{
                                    user_id:res.data.child[i].user_id
                                },
                                success : function(resx) {
                                    if(resx.status == 0){
                                        swal("Error",resx.message,"error");
                                    }else{
                                    appendMonthly(resx.data, 2,true);
                                        for(var i = 0;i<resx.data.child.length;i++){
                                            $.ajax({
                                                type: 'POST',
                                                url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                                dataType: 'json',
                                                data:{
                                                    user_id:resx.data.child[i].user_id
                                                },
                                                success : function(resx) {
                                                    if (resx.status == 0) {
                                                        swal("Error", resx.message, "error");
                                                    } else {
                                                        appendMonthly(resx.data, 3,true);
                                                        for(var i = 0;i<resx.data.child.length;i++){
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                                                dataType: 'json',
                                                                data:{
                                                                    user_id:resx.data.child[i].user_id
                                                                },
                                                                success : function(resx) {
                                                                    if (resx.status == 0) {
                                                                        swal("Error", resx.message, "error");
                                                                    } else {
                                                                        appendMonthly(resx.data, 4,false);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }catch (err){
                            console.log(err);
                        }
                    }
                });
            }
            <?php }else{?>
            changeParent()
            function changeParent() {
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                    dataType: 'json',
                    data:{
                        user_id:"<?= $_SESSION['ksbid'] ?>"
                    },
                    success : function(res){
                        try{
                            appendMonthly(res.data,1,true);
                            for(var i = 0;i<res.data.child.length;i++)
                                $.ajax({
                                    type: 'POST',
                                    url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                    dataType: 'json',
                                    data:{
                                        user_id:res.data.child[i].user_id
                                    },
                                    success : function(resx) {
                                        if(resx.status == 0){
                                            swal("Error",resx.message,"error");
                                        }else{
                                            appendMonthly(resx.data, 2,true);
                                            for(var i = 0;i<resx.data.child.length;i++){
                                                $.ajax({
                                                    type: 'POST',
                                                    url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                                    dataType: 'json',
                                                    data:{
                                                        user_id:resx.data.child[i].user_id
                                                    },
                                                    success : function(resx) {
                                                        if (resx.status == 0) {
                                                            swal("Error", resx.message, "error");
                                                        } else {
                                                            appendMonthly(resx.data, 3,true);
                                                            for(var i = 0;i<resx.data.child.length;i++){
                                                                $.ajax({
                                                                    type: 'POST',
                                                                    url: "{!! url('/api/v1/getMonthlyBonusTree') !!}",
                                                                    dataType: 'json',
                                                                    data:{
                                                                        user_id:resx.data.child[i].user_id
                                                                    },
                                                                    success : function(resx) {
                                                                        if (resx.status == 0) {
                                                                            swal("Error", resx.message, "error");
                                                                        } else {
                                                                            appendMonthly(resx.data, 4,false);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                        }catch (err){
                            console.log(err);
                        }
                    }
                });
            }
            <?php } ?>



            function appendMonthly(data,level,clicked) {
                var modelnew = $g(go.TreeModel);
                if(level === 1){
                    dataArr.push({
                        key: data.user_id,
                        text: data.name + "\n" +data.id,
                        source: "{{url("/uploads/users/")}}/" + data.picture
                    });
                }
                for (var i = 0; i < data.child.length; i++) {
                    dataArr.push({
                        key: data.child[i].user_id,
                        parent: data.user_id,
                        text: data.child[i].name + "\n" +data.child[i].id,
                        level: (level+1),
                        clicked: clicked,
                        source: "{{url("/uploads/users/")}}/" + data.child[i].picture
                    });
                }
                modelnew.nodeDataArray = dataArr;
                myDiagram.model = modelnew;
            }

        });
    </script>
@endsection