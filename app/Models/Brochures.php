<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brochures extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brochures';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'picture',
        'type',
        'created_at',
        'updated_at'
    ];
}
