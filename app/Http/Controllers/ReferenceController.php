<?php
/*DEVELOPING BY Gandhy Bagoes Cahyono*/
namespace App\Http\Controllers;

use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use Illuminate\Support\Facades\Input;
use App\Models\Branch;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\User;
use Illuminate\Http\Request;
use App\Core\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReferenceController extends Controller
{
    public function getUserReferalOption()
    {
        try 
        {
            $data = DB::table(Constants::USERS)
                ->select(
                    Constants::SALT ." as user_id",
                    Constants::NAME,
                    Constants::REF_CODE,
                    Constants::MEMBER_DETAILS.".".Constants::ID . " as member_num"
                    )
                ->join(Constants::MEMBER_DETAILS, Constants::USERS.".".Constants::SALT, "=", Constants::MEMBER_DETAILS.".".Constants::USER_ID)
                ->where(Constants::USERS.".".Constants::STATUS, Constants::STATUS_ACTIVE)
                ->where(Constants::MEMBER_DETAILS.".".Constants::REF_QUOTA, "!=", 0)
                ->get();
            if (null == $data) throw new VerificationException("Tidak ada user yang memiliki reference");
            return Response::success($data);
        }
        catch(VerificationException $ve)
        {
            return Response::failed($ve->getMessage());
        }
        catch (\Exception $e)
        {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

}
