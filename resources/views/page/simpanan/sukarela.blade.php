@extends('templates.index')
@section('title')
    Simpanan Sukarela
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>

    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Simpanan Sukarela</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Simpan Pinjam</a>
            </li>
            <li class="active">Sukarela</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Tabel Member</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="member_table">
                        <thead>
                        <tr>
                            <th> KTP Anggota</th>
                            <th> Nama Angggota</th>
                            <th> Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>



    <!-- MODAL VIEW SAVING -->
    <div class="modal fade bs-modal-lg" id="view_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="modal_judul">Tabel Simpanan Sukarela</h4>
                </div>
                <form id="form" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Periode</label>
                                <div class="col-md-3">
                                    <select id="view_bulan" class="form-control" name="view_bulan">
                                        <option value="">-- Bulan --</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select id="view_tahun" class="form-control" name="view_tahun">
                                        <option value="">-- Tahun --</option>
                                        <option value="2018">2018</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" class="btn dark btn-outline" id="view_button_cari"
                                            data-dismiss="modal">
                                        CARI
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="portlet-body">
                            <table class="table view-table table-striped table-bordered table-hover table-checkable order-column"
                                   id="view_table">
                                <thead>
                                <tr>
                                    <th> Tanggal</th>
                                    <th> Jumlah</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>00-00-0000</td>
                                    <td>Rp. 100.000</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" id="modal_button_close" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL VIEW SAVING SELESAI -->



    <!-- MODAL TAMBAH SAVING -->
    <div class="modal fade bs-modal-lg" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="modal_judul">Simpanan Sukarela</h4>
                </div>
                <form id="form" class="form-horizontal">
                    <input type="hidden" id="user_id" value="">
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jumlah Simpanan</label>
                                <div class="col-md-9">
                                    <select id="add_amount" class="form-control select2" name="add_amount">
                                        <option value="500000">Rp. 500.000</option>
                                        <option value="1000000">Rp. 1.000.000</option>
                                        <option value="1500000">Rp. 1.500.000</option>
                                        <option value="2000000">Rp. 2.000.000</option>
                                        <option value="2500000">Rp. 2.500.000</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" id="modal_button_close" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="modal-button btn green" id="add_button_save">Simpan
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL TAMBAH SAVING SELESAI -->
@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function () {

            /*$('.btn-view').click(function () {
                $('#view_modal').modal('show');
            });
            $('.btn-add').click(function () {
                $('#add_modal').modal('show');
            });*/
            /* READ MEMBER */
            //Menampilkan data member ke tabel mamber
            var table = $('#member_table').DataTable({
                dom: "Bfrtip",
                ajax: {
                    "url": "{{ route('get.member.saving') }}",
                    "type": "POST"
                },
                columns: [
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<h4 align='center'>" + full.user_ktp + "</h4>";
                            return res;
                        }
                    },
                    {
                        sortable: true,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<h4 align='center'>" + full.user_name + "</h4>";
                            return res;
                        }
                    },
                    {
                        sortable: false,
                        "render": function (data, type, full, meta) {
                            var res = "";
                            res += "<div align='center'>";
                            res += "<a href='javascript:;' id='view_" + full.user_id + "' x-data='" + full.user_id + "' class=\"btn btn-default btn-view\"><span class=\"glyphicon glyphicon-eye-open\"></span> View </a>";
                            res += "<a href='javascript:;' id='add_" + full.user_id + "' x-data='" + full.user_id + "' class=\"btn btn-primary btn-add\"><span class=\"glyphicon glyphicon-plus-sign\"></span> Add </a>";
                            res += "</div>";
                            return res;
                        }
                    }
                ],
                select: true,
                buttons: []
            });

            /* VIEW SIMPANAN */
            //Menampilkam modal View Simpanan
            $('#member_table').on('click', '.btn-view', function () {
                $('#view_modal').modal('show');
                var member_id = $(this).attr('x-data').replace(/\'/g, "\"");
                $('.view-table').DataTable({
                    dom: "Bfrtip",
                    ajax: {
                        "url": "{{ route('get.byid.saving.sukarela') }}",
                        "type": "POST",
                        "data": {user_id: member_id}
                    },
                    columns: [
                        {data: "tanggal"},
                        {data: "amount"}
                    ],
                    select: true,
                    buttons: []
                });
            });

            /* ADD SIMPANAN */
            //Menampilkam modal Add Simpanan
            $('#member_table').on('click', '.btn-add', function () {
                var member_id = $(this).attr('x-data').replace(/\'/g, "\"");
                $('#add_modal').modal('show');
                $('#user_id').val(member_id);
            });
            //Menyimpan Simpanan ke database sesuai user_id
            $('#add_modal').on('click', '#add_button_save', function () {
                $.ajax({
                    type: 'POST',
                    url: "{!! route('add.saving.sukarela') !!}",
                    dataType: 'json',
                    data: {
                        user_id: $("#user_id").val(),
                        amount: $("#add_amount").val(),
                    },
                    success: function (res) {
                        $("#add_modal").modal("hide");

                        var title = "Error!";
                        var content = res.message;
                        var tag = "error";
                        if (res.status === 1) {
                            title = "Berhasil Simpan!";
                            content = res.data.message;
                            tag = "success";
                            table.ajax.reload(null, false);
                            swal({
                                title: title,
                                text: content,
                                type: tag
                            }).then(function () {
                                table.ajax.reload(null, false);
                                //window.location.reload();
                                $('#user_id').val("");
                                $('#add_amount').val("");
                            });
                        } else {
                            swal({
                                title: title,
                                text: content,
                                type: tag
                            }).then(function () {
                                $("#add_modal").modal("show");
                            });
                        }
                    }
                });
            });



        })
    </script>
@endsection
