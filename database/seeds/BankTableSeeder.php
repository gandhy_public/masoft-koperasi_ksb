<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        DB::table('banks')->insert(
            [
                'id'=>'1',
                'short_name' =>  "BCA",
                'code_bank' => '110',
                'full_name' => "Bank Central Asia",
                'created_at' => date('Y-m-d H:i:s')

            ]
        );
        DB::table('banks')->insert(
            [
                'id'=>'2',
                'short_name' =>  "BNI",
                'code_bank' => '111',
                'full_name' => "Bank Negara Indonesia",
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
        //Bank Account
        DB::table('bank_account')->insert(
            [
                'id'=>'1',
                'user_id' =>  '$49213jfdsajkk.4KSKJTestSalt1',
                'bank_id' => '1',
                'type' => 'member',
                'account_number' => "123412431234",
                'account_name' => "Pak Anggota 1",
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
        DB::table('bank_account')->insert(
            [
                'id'=>'2',
                'user_id' =>  '1',
                'bank_id' => '2',
                'type' => 'koperasi',
                'account_number' => "123412431234",
                'account_name' => "KSB TANGERANG",
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
    }
}
