<?php
/**
 * Created by PhpStorm.
 * User: TOSHIBA
 * Date: 11/04/2018
 * Time: 14:21
 */

namespace App\Http\Controllers;

use App\Core\Constants;
use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

Class LoanController extends Controller
{


    public function getLoans()
    {
        try {
            $data = DB::table("loans");

            if ("Member" == $_SESSION['ksbrole']) {
                $data->where(Constants::USER_ID, $_SESSION['ksbid']);

                if ($data->count() == 0) throw new VerificationException("data pinjaman tidak ada!");
                return Response::table($data->get());
            } elseif ($_SESSION['ksbrole'] == "Super Admin" || $_SESSION['ksbrole'] == "Admin") {
                $data->join('users', 'loans.user_id', '=', 'users.salt')
                    ->select('loans.*', 'users.name');
                if ($data->count() == 0) throw new VerificationException("data pinjaman tidak ada!");
                return Response::table($data->get());
            } else {
                throw new VerificationException("role tidak diizinkan!");
            }

        } catch (VerificationException $le) {
            return Response::failed($le->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function addLoans()
    {
        try {
            Verification::check("amount", "%key% Tidak boleh kosong!");
            $data = DB::table("loans");

            if ($_SESSION['ksbrole'] == "Member") {
                $data->insert([
                    'user_id' => $_SESSION['ksbid'],
                    'status' => "pending",
                    'amount' => Input::get("amount"),
                    'periode' => "",
                    'nomor_pinjaman' => "",
                    'type' => "",
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                return Response::success("Request Anda berhasil terkirim, silahkan menunggu konfirmasi dari ADMIN!");
            } else {
                throw new VerificationException("role tidak diizinkan!");
            }
        } catch (VerificationException $le) {
            return Response::failed($le->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function aksiLoans()
    {
        try {
            Verification::check("loan_id", "ID tidak boleh kosong!");
            Verification::check("aksi", "Method tidak boleh kosong!");
            $data = DB::table("loans");

            if (Input::get("aksi") == "approve" && $_SESSION['ksbrole'] != "Member") {
                $kode = "KSB-" . rand();
                $data->where('id', Input::get("loan_id"))
                    ->update([
                        Constants::STATUS => "approved",
                        'nomor_pinjaman' => $kode,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                return Response::success("Pinjaman berhasil disetuji!");
            } elseif (Input::get("aksi") == "reject" && $_SESSION['ksbrole'] != "Member") {
                $kode = "canceled";
                $data->where('id', Input::get("loan_id"))
                    ->update([
                        Constants::STATUS => "rejected",
                        'nomor_pinjaman' => $kode,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                return Response::success("Pinjaman ditolak!");
            } elseif (Input::get("aksi") == "paid" && $_SESSION['ksbrole'] != "Member") {
                $data->where('id', Input::get("loan_id"))
                    ->update([
                        Constants::STATUS => "paid",
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                return Response::success("Pinjaman sudah lunas!");
            } else {
                throw new VerificationException("role tidak diizinkan!");
            }
        } catch (VerificationException $le) {
            return Response::failed($le->getMessage());
        } catch (\Exception $e) {
            //return Response::failed($e->getMessage());
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }


}
