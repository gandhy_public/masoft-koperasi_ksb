<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 1/23/18
 * Time: 8:36 PM
 */

namespace App\Core;


class Response extends Core
{

    public static function success($data){
        return json_encode(array(Constants::STATUS=>Constants::STATUS_SUCCESS,Constants::DATA=>$data));
    }
    public static function table($data){
        $i =1;
        foreach ($data as $d){
            $d->DT_RowId = "row_" . $i;
            $i++;
        }
        $res['data'] = $data;
        $res["options"] = array();
        $res["files"] = array();
        return json_encode($res);
    }

    public static function failed($message){
        return json_encode(array(Constants::STATUS=>Constants::STATUS_FAILED,Constants::MESSAGE=>$message));
    }

}