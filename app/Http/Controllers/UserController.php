<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 1/23/18
 * Time: 7:53 AM
 */

namespace App\Http\Controllers;


use App\Core\Constants;
use App\Core\Response;
use App\Core\Verification;
use App\Exceptions\VerificationException;
use App\Models\BankAccount;
use App\Models\MemberDetail;
use App\Models\Monthly;
use App\Models\Referal;
use App\Models\Reference;
use App\Models\Sponsorship;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{

    public function logout()
    {
        session_start();
        session_destroy();
        return redirect('/login');
    }

    public function getUserDetails()
    {
        try {
            Verification::check(Constants::USER_ID, "%key% tidak boleh kosong.");
            $users = User::where(Constants::SALT, Input::get(Constants::USER_ID));
            if($users->count() == 0) throw new VerificationException(Constants::ERROR_NO_USERS_DATA);
            return Response::success($users->first());
        }catch (VerificationException $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function login()
    {
        try {
            Verification::check(Constants::USERNAME,"%key% tidak boleh kosong.");
            Verification::check(Constants::PASSWORD,"%key% tidak boleh kosong.");
            $users = User::where(Constants::USERNAME, Input::get(Constants::USERNAME));
            if ($users->count() == 0 || null == $users) throw new VerificationException(Constants::ERROR_USER_NOT_FOUND);
            if (!password_verify(Input::get(Constants::PASSWORD), $users->get()[0]->password)) throw new VerificationException(Constants::ERROR_PASSWORD_NOT_VALID);
            $users = $users->get()[0];
            if($users->status != Constants::STATUS_ACTIVE) throw new VerificationException("Your user status is " + $users->status);
            $users->update([
                Constants::SESSION_ID => md5(date('yyyymmddhhiiss').Input::get(Constants::USERNAME).rand(100000,99999999))
            ]);
            session_start();
            $_SESSION[Constants::NAME] = $users->name;
            $_SESSION[Constants::MONEY] = $users->balance;
            $_SESSION[Constants::SESSION_USER_ID] = $users->salt;
            $_SESSION[Constants::SESSION_ROLE] = $users->role;
            return Response::success($users);
        }catch (VerificationException $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function setStatus()
    {
        try {
            Verification::check(Constants::USER_ID);
            Verification::check(Constants::STATUS);
            $user = User::where(Constants::SALT,Input::get(Constants::USER_ID));
            if($user->count() == 0) throw new VerificationException(Constants::ERROR_NO_USERS_DATA);
            $user->update([
                Constants::STATUS => Input::get(Constants::STATUS)
            ]);
            return Response::success(Constants::MESSAGE_SUCCESS_STATUS_SET);
        }catch (VerificationException
        $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function getStatus()
    {
        try {
            Verification::check(Constants::USER_ID);
            $user = User::where(Constants::SALT,Input::get(Constants::USER_ID));
            if($user->count() == 0) throw new VerificationException(Constants::ERROR_NO_USERS_DATA);
            return Response::success($user->first()->status);
        }catch (VerificationException
        $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function editProfile(){
        try {
            Verification::check(Constants::USER_ID);
            Verification::check(Constants::NAME);
            Verification::check(Constants::OLD_PASSWORD);
            Verification::check(Constants::NEW_PASSWORD);
            Verification::check(Constants::PHONE);
            Verification::check(Constants::ADDRESS);
            $usr = User::where(Constants::SALT , Input::get(Constants::USER_ID));
            if ($usr->count() == 0 || null == $usr) throw new VerificationException(Constants::ERROR_NO_USERS_DATA);
            if(!password_verify(Input::get(Constants::OLD_PASSWORD),$usr->first()->password)) throw new VerificationException(Constants::MESSAGE_FAILED_PASSWORD_NOT_MATCH);
            $usr->update([
                Constants::NAME=>Input::get(Constants::NAME),
                Constants::PASSWORD=>password_hash(Input::get(Constants::NEW_PASSWORD),PASSWORD_BCRYPT),
                Constants::PHONE=>Input::get(Constants::PHONE),
                Constants::ADDRESS=>Input::get(Constants::ADDRESS)
            ]);

            return Response::success(Constants::MESSAGE_SUCCESS_EDIT_PROFILE);
        }catch (VerificationException
        $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function edit(){
        try {
            Verification::check(Constants::USER_ID);
            Verification::check(Constants::NAME);
            Verification::check(Constants::USERNAME);
            Verification::check(Constants::PHONE);
            Verification::check(Constants::ADDRESS);
            Verification::check(Constants::EMAIL);
            $usr = User::where(Constants::SALT , Input::get(Constants::USER_ID));
            if ($usr->count() == 0 || null == $usr) throw new VerificationException("User not found.");
            $usr->update([
                Constants::NAME=>Input::get(Constants::NAME),
                Constants::PHONE=>Input::get(Constants::PHONE),
                Constants::USERNAME=>Input::get(Constants::USERNAME),
                Constants::ADDRESS=>Input::get(Constants::ADDRESS),
                Constants::EMAIL=>Input::get(Constants::EMAIL)
            ]);
            return Response::success("Your have successfully edit.");
        }catch (VerificationException $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }
    public function addMember(){
        try {
            Verification::check(Constants::USER_REFERAL , Constants::SERVER_FAILURE);
            Verification::check(Constants::NAME , "Nama harus diisi!");
            Verification::check(Constants::EMAIL, "Email harus diisi!");
            Verification::check(Constants::PASSWORD, "Password harus diisi!");
            Verification::check(Constants::CONFIRM_PASSWORD, "Konfirmasi Password harus diisi!");
            Verification::check(Constants::USERNAME, "Username harus diisi!");
            Verification::check(Constants::PHONE, "Nomor Telpon harus diisi!");
            Verification::check(Constants::ADDRESS, "Alamat harus diisi!");
            Verification::check(Constants::BIRTH_PLACE, "Tempat Lahir harus diisi!");
            Verification::check(Constants::BIRTH_DATE, "Tanggal Lahir harus diisi!");
            Verification::check(Constants::PICTURE, "Gambar harus diisi!");
            Verification::check(Constants::IDENTITY_TYPE, "Jenis Identitas harus diisi!");
            Verification::check(Constants::IDENTITY_NUM, "Nomor Identitas harus diisi!");
            Verification::check(Constants::MOTHER_NAME, "Nama Ibu harus diisi!");
            Verification::check(Constants::FIRST_HEIR, "Ahli Waris Pertama harus diisi!");
            Verification::check(Constants::SECOND_HEIR, "Ahli Waris Kedua harus diisi!");
            Verification::check(Constants::JOB_NAME, "Pekerjaan harus diisi!");
            Verification::check(Constants::BANK_ID, "Bank harus Dipilih!");
            Verification::check(Constants::ACCOUNT_NAME, "Nama Rekening harus diisi!");
            Verification::check(Constants::ACCOUNT_NUMBER, "Nomor Rekening harus diisi!");
            Verification::check(Constants::PICTURE, "Gambar harus diisi!");
            if (strpos(Input::get(Constants::USERNAME), ' ') !== false) throw new VerificationException("Username tidak boleh menggunakan spasi");
            $salt = password_hash(Input::get(Constants::USERNAME) . Input::get(Constants::PASSWORD),PASSWORD_BCRYPT);
            if(Input::get(Constants::PASSWORD) != Input::get(Constants::CONFIRM_PASSWORD)) throw new VerificationException("Password not match");
            if(User::where([Constants::USERNAME => Input::get(Constants::USERNAME)])->count() !=0) throw new VerificationException("Username already exist.");
            if(User::where([Constants::EMAIL => Input::get(Constants::EMAIL)])->count() !=0) throw new VerificationException("Email already exist.");

            User::create([
                Constants::NAME => Input::get(Constants::NAME),
                Constants::EMAIL => Input::get(Constants::EMAIL),
                Constants::PASSWORD => password_hash(Input::get(Constants::PASSWORD),PASSWORD_BCRYPT),
                Constants::USERNAME => Input::get(Constants::USERNAME),
                Constants::PHONE => Input::get(Constants::PHONE),
                Constants::ADDRESS => Input::get(Constants::ADDRESS),
                Constants::PICTURE => Input::get(Constants::PICTURE),
                Constants::ROLE => 'Member',
                Constants::SALT => $salt,
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
            ]);

            $birth_date_exp = explode("/",Input::get(Constants::BIRTH_DATE));
            $birth_date = $birth_date_exp[2]."-".$birth_date_exp[0]."-".$birth_date_exp[1]." 00:00:01";
            $ref_code = substr(Input::get(Constants::USERNAME),0,8).$birth_date_exp[1].$birth_date_exp[0].rand(0,10);
            MemberDetail::create([
                Constants::USER_ID => $salt,
                Constants::REF_CODE => $ref_code,
                Constants::BIRTH_PLACE => Input::get(Constants::BIRTH_PLACE),
                Constants::BIRTH_DATE => $birth_date,
                Constants::IDENTITY_NUM => Input::get(Constants::IDENTITY_NUM),
                Constants::IDENTITY_TYPE => Input::get(Constants::IDENTITY_TYPE),
                Constants::MOTHER_NAME => Input::get(Constants::MOTHER_NAME),
                Constants::FIRST_HEIR => Input::get(Constants::FIRST_HEIR),
                Constants::SECOND_HEIR => Input::get(Constants::SECOND_HEIR),
                Constants::BRANCH_ID => Input::get(Constants::BRANCH_ID),
                Constants::JOB_NAME => Input::get(Constants::JOB_NAME),
                Constants::TYPE => "member",
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
            ]);


            BankAccount::create([
                Constants::USER_ID => $salt,
                Constants::BANK_ID => Input::get(Constants::BANK_ID),
                Constants::ACCOUNT_NUMBER => Input::get(Constants::ACCOUNT_NUMBER),
                Constants::ACCOUNT_NAME => Input::get(Constants::ACCOUNT_NAME),
                Constants::TYPE => "member",
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
            ]);

            $parent_id = explode(";",Input::get(Constants::USER_REFERAL))[0];
            $parent_ref_code = explode(";",Input::get(Constants::USER_REFERAL))[1];

            $parent_quota = MemberDetail::select(Constants::REF_QUOTA)
                ->where(Constants::USER_ID,$parent_id)
                ->first()->ref_quota;

            $ref_no_max = Reference::select(DB::raw("max(" . Constants::REF_NO .") as max"))
                ->where(Constants::REF_CODE,$parent_ref_code)
                ->first()->max;
            $ref_no_max = null == $ref_no_max?1:($ref_no_max+1);

            if($ref_no_max>=intval($parent_quota)+1) throw new VerificationException("Anda tidak bisa menggunakan referal user ini, dikarenakan kuota penuh.");

            $ref = Reference::create([
                Constants::REF_CODE =>$parent_ref_code,
                Constants::REF_NO =>$ref_no_max,
                Constants::USER_ID =>$salt,
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
            ]);

            Sponsorship::create([
                Constants::PARENT_ID =>$parent_id,
                Constants::REF_ID => $ref->id,
                Constants::STATUS =>"active",
                Constants::INDEX_NUM =>0,
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
                ]);

            $found_position = false;
            $parent_for_monthly = array($parent_id);
            $used_parent_id ="";
            $i=0;
            while(!$found_position){
                $now_sp = Monthly::select(DB::raw("count(" . Constants::ID .") as count"))
                    ->where(Constants::PARENT_ID,$parent_for_monthly[$i])
                    ->first()->count;
                if($now_sp >= 2 ){
                    if($i < count($parent_for_monthly)-1){
                        $i++;
                    }else{
                        $datas = DB::table(Constants::REFERENCES)
                            ->select(Constants::REFERENCES.".".Constants::USER_ID ." as user_id")
                            ->join(Constants::MEMBER_DETAILS,
                                Constants::REFERENCES.".".Constants::REF_CODE,"=",Constants::MEMBER_DETAILS.".".Constants::REF_CODE)
                            ->where(Constants::MEMBER_DETAILS.".".Constants::USER_ID,$parent_for_monthly[$i])
                            ->get();
                        $parent_for_monthly = [];
                        foreach ($datas as $data){
                            array_push($parent_for_monthly,$data->user_id);
                        }
                        $i=0;
                    }
                }else{
                    $found_position = true;
                    $used_parent_id = $parent_for_monthly[$i];
                }
            }
            Monthly::create([
                Constants::PARENT_ID =>$used_parent_id,
                Constants::REF_ID => $ref->id,
                Constants::STATUS =>"active",
                Constants::INDEX_NUM =>0,
                Constants::CREATED_AT =>date('Y-m-d H:i:s')
            ]);

            return Response::success("Your have successfully add user.");
        }catch (VerificationException
        $le){
            return Response::failed($le->getMessage());
        }catch (\Exception $e){
            return Response::failed($e->getMessage());
        }
    }
    public function uploadUserPicture(){
        try {
            $fileName = time() . "_" . rand(1000,9999) . "_user";
            $fileType = explode('.', $_FILES["file"]["name"]);
            $fileType = strtolower($fileType[count($fileType)-1]);
            $fileError = $_FILES['file']['error'];
            $fileContent = file_get_contents($_FILES['file']['tmp_name']);
            if(!in_array($fileType, array("jpg","jpeg","png"))) throw new VerificationException("File must be image ");
            if($fileError == UPLOAD_ERR_OK){
                file_put_contents(public_path() . "/uploads/users/" . $fileName.".".$fileType, $fileContent);
                return  Response::success(array("message"=>"File uploaded!","file_name"=>$fileName.".".$fileType));
            }else{
                switch($fileError){
                    case UPLOAD_ERR_INI_SIZE:
                        throw new VerificationException('Error INI FILE SIZE');
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new VerificationException('Error UPLOAD_ERR_FORM_SIZE');
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        throw new VerificationException('Error: UPLOAD_ERR_PARTIAL');
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new VerificationException('Error: UPLOAD_ERR_NO_FILE');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        throw new VerificationException('Error: UPLOAD_ERR_NO_TMP_DIR');
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        throw new VerificationException('Error: UPLOAD_ERR_CANT_WRITE');
                        break;
                    case  UPLOAD_ERR_EXTENSION:
                        throw new VerificationException('Error: UPLOAD_ERR_EXTENSION');
                        break;
                    default: throw new VerificationException('Error: Unkown file error.');
                        break;
                }
            }
        }catch(VerificationException $ve){
            return Response::failed($ve->getMessage());
        }catch (QueryException $qe){
            return Response::failed("Cannot import data, please recheck your data");
        }catch (\Exception $e) {
            return Response::failed(Constants::SERVER_FAILURE);
        }
    }

    public function deleteUser(){
        session_start();
        if ($_SESSION[Constants::SESSION_ROLE] == 'Member') {
            return Response::failed("Prohibitted");
        } else {
            try {
                Verification::check(Constants::USER_ID);
                $u = User::where(Constants::SALT,Input::get(Constants::USER_ID));
                if($u->count() == 0) throw new VerificationException(Constants::ERROR_NO_USERS_DATA);
                $u->delete();
                return Response::success("Delete success.");
            }catch (VerificationException $le){
                return Response::failed($le->getMessage());
            }catch (\Exception $e){
                return Response::failed(Constants::SERVER_FAILURE);
            }
        }
    }
}