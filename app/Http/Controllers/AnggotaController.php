<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Core\Constants;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AnggotaController extends Controller
{
    public function dataAnggota()
    {
        session_start();
        if (!isset($_SESSION[Constants::SESSION_USER_ID])) {
            return redirect()->route('login');
        } else {
            if ($_SESSION[Constants::SESSION_ROLE] == 'Member') {
                return redirect()->route('home');
            } else {
                $title = 'Anggota';
                $data = User::all();
                return view('page.anggota', compact('title', 'data'));
            }
        }
    }

    public function bonusBulanan()
    {
        session_start();
        if (!isset($_SESSION[Constants::SESSION_USER_ID])) {
            return redirect()->route('login');
        } else {
            $title = 'Bonus Bulanan';
            return view('page.bulanan', compact('title'));
        }
    }
}
