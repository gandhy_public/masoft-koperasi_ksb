@extends('templates.index')
@section('title')
    Bonus Perekrutan
@endsection
@section('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <style>
        .thumbnail{
            border-radius: 50%!important;
        }
        .thumbnail img{
            border-radius: 50%!important;
        }
        .detail-label{
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin-bottom: 10px !important;
            margin-top: 0 !important;
        }
        .select2{
            z-index: 98!important;
        }
    </style>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="breadcrumbs">
        <h1>Bonus Perekrutan</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Bonus</a>
            </li>
            <li class="active">Perekrutan</li>
        </ol>
    </div>
    <hr>
    <div class="row">
        <div class="alert alert-info" id="notif" style="display: none;"></div>
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered" id="child-container">
                <div class="portlet-title">
                        <div class="caption">
                                <span  class="caption-subject font-green-sharp bold uppercase">Diagram Bonus Perekrutan</span>
                        </div>
                </div>
                <div class="portlet-body">
                    <h4 class="block"></h4>
                    <?php if($_SESSION['ksbrole'] == "Admin" ||$_SESSION['ksbrole'] == "Super Admin"){?>
                    <div class="row">
                        <div class="col-sm-12 col-md-offset-4 col-md-4">
                            <select id="select_user" class="form-control select2">
                                <option value="---">Loading...</option>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-12 col-md-offset-5 col-md-2">

                            <br>
                            <a href="javascript:;" class="thumbnail">
                                <img id="parent_picture" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NyIgaGVpZ2h0PSIxODAiPjxyZWN0IHdpZHRoPSI2NyIgaGVpZ2h0PSIxODAiIGZpbGw9IiNlZWUiLz48dGV4dCB0ZXh0LWFuY2hvcj0ibWlkZGxlIiB4PSIzMy41IiB5PSI5MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjY3eDE4MDwvdGV4dD48L3N2Zz4=" alt="100%x180" style="height: 12vw; width: 100%; display: block;" data-src="../assets/global/plugins/holder.js/100%x180"> </a>
                            <center>
                                <h4 class="block detail-label" id="parent_name"><strong>Pak member</strong></h4>
                                <h4 class="block detail-label" id="parent_number">0001</h4>
                            </center>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>


@endsection

@section('js')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{--<script src="/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){
            <?php if($_SESSION['ksbrole'] == "Admin" ||$_SESSION['ksbrole'] == "Super Admin"){?>
            $.ajax({
                url: '{{ route('get.user.referal.option') }}',
                data: {},
                type: 'post',
                dataType: 'json',
                success: function(res) {
                    users = "";
                    for(var i = 0;i<res.data.length;i++){
                        users += '<option value="' + res.data[i].user_id + '">[ ' + res.data[i].ref_code  + ' - '+ res.data[i].member_num +' ] ' + res.data[i].name + '</option>'
                    }
                    $('#select_user').html(users);
                    $('#select_user').select2({
                        width:"100%"
                    });
                    changeParent()
                },
                error: function(xhr) {
                    alert("Server Error!")
                }
            });
            $('#select_user').change(function () {
                changeParent();
            });
            function changeParent() {
                $('.child-portlet').remove();
                $.ajax({
                    type: 'POST',
                    url: "{!! url('/api/v1/getSponsorshipBonusTree') !!}",
                    dataType: 'json',
                    data:{
                        user_id:$('#select_user').val()
                    },
                    success : function(res){
                        try{
                            $('#parent_picture').attr("src", '{{url("/uploads/users")}}/' + res.data.parent.picture);
                            $('#parent_name').html("<strong>"+res.data.parent.name+"</strong>");
                            $('#parent_number').html(res.data.parent.id);
                            appendSponsorship(res.data,1);
                        }catch (err){
                            console.log(err);
                        }
                    }
                });
            }
            <?php }else{ ?>
            $.ajax({
                type: 'POST',
                url: "{!! url('/api/v1/getSponsorshipBonusTree') !!}",
                dataType: 'json',
                data:{
                    user_id:"<?= $_SESSION['ksbid'] ?>"
                },
                success : function(res){
                    try{
                        $('#parent_picture').attr("src", '{{url("/uploads/users")}}/' + res.data.parent.picture);
                        $('#parent_name').html(res.data.parent.name);
                        $('#parent_number').html(res.data.parent.id);
                        appendSponsorship(res.data,1);
                    }catch (err){
                        console.log(err);
                    }
                }
            });
            <?php } ?>
            $('#child-container').on('click','.child-frame',function () {
                var lv = $(this).attr('x-data-level');
                if(parseInt(lv) < 5) {
                    $.ajax({
                        type: 'POST',
                        url: "{!! url('/api/v1/getSponsorshipBonusTree') !!}",
                        dataType: 'json',
                        data: {
                            user_id: $(this).attr('x-data-user-id')
                        },
                        success: function (res) {
                            try {
                                appendSponsorship(res.data, (parseInt(lv) + 1));
                            } catch (err) {
                                console.log(err);
                            }
                        }
                    });
                }
            });
            function appendSponsorship(data,level) {
                if(level <= 5) {
                    var out = "";
                    for(var i = level;i<5;i++){
                        $('#container_lv_' + i).remove();
                    }
                    if (!$("#container_lv_" + level).length) {
                        if (data.child.length != 0) {
                            out += '<div class="portlet-body child-portlet" id="container_lv_' + level + '">';
                            out += '<h4 class="block">Tingkat ' + level + '</h4>';
                            out += '<div class="row" id="container_body_lv_' + level + '">';
                        }
                    }
                    for (var i = 0; i < data.child.length; i++) {
                        var offset = i == 0 ? 'col-md-offset-1 ' : '';
                        out += '<div class="col-sm-12 ' + offset + 'col-md-2">';
                        out += '<a href="javascript:;" x-data-level="' + level + '" x-data-user-id="' + data.child[i].user_id + '" class="child-frame thumbnail">';
                        out += '<img src="{{url("/uploads/users")}}/' + data.child[i].picture + '" id="child_' + level + '_' + (i + 1) + '_picture" alt="100%x180" style="height: 12vw; width: 100%; display: block;"></a>';
                        out += '<center>';
                        out += '<h4 class="block detail-label" id="child_' + level + '_' + (i + 1) + '_name"><strong>' + data.child[i].name + '</strong></h4>';
                        out += '<h4 class="block detail-label" id="child_' + level + '_' + (i + 1) + '_number">' + data.child[i].id + '</h4>';
                        out += '</center>'
                        out += '</div>'
                    }
                    if (!$("#container_lv_" + level).length) {
                        if (data.child.length != 0) {
                            out += '</div></div>';
                            $('#child-container').append(out);
                        }
                    } else {
                        $("#container_body_lv_" + level).html(out);
                    }
                }
            }

        });
    </script>
@endsection