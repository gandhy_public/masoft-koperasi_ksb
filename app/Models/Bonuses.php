<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonuses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bonuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'amount',
        'status',
        'type',
        'created_at',
        'updated_at'
    ];
}
