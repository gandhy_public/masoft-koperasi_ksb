<?php
/**
 * Created by PhpStorm.
 * User: vergieet
 * Date: 2/1/18
 * Time: 9:43 PM
 */

namespace App\Core;

use App\Exceptions\VerificationException;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\View;

class Auth
{
    public static function check($to,$type = "controller",$role="Member")
    {
        session_start();
        try {
            if (empty($_SESSION[Constants::SESSION_USER_ID])) {
                Verification::check(Constants::SESSION_USER_ID,"Tidak ada sesi tersedia, silahkan login!");
                $usr = User::where(Constants::SESSION_ID, Input::get(Constants::SESSION_ID));
                if ($usr->count() == 0) throw new \Exception("No users on db with this session_id");
                $_SESSION[Constants::SESSION_ROLE] = $usr->first()->role;
                $_SESSION[Constants::SESSION_USER_ID] = $usr->first()->salt;
            } else {
                if(empty($_SESSION[Constants::SESSION_ROLE]))throw new \Exception("Not logged in");
            }
            if($role =="All") {
                //do nothing
            }elseif($role =="Admin"){
                if ($_SESSION[Constants::SESSION_ROLE] != "Super Admin" && $_SESSION[Constants::SESSION_ROLE] != "Admin") throw new \Exception("Not Authorized");
            }elseif($role =="Super Admin"){
                if ($_SESSION[Constants::SESSION_ROLE] != "Super Admin") throw new \Exception("Not Authorized");
            }elseif($role =="Member"){
                if ($_SESSION[Constants::SESSION_ROLE] != "Member") throw new \Exception("Not Authorized");
            }else{
                throw new \Exception("Unknown role");
            }


            if ($type == "view") {
                return View::make($to);
            } elseif ($type == "controller") {
                return App::call("App\Http\Controllers\\" . $to);

            } else {
                throw new \Exception("wrong type");
            }
        } catch (\Exception $e) {
//            session_destroy();
            return Response::failed($e->getMessage());
        }
    }
    public static function sucheck($to,$type = "view"){
            return self::check($to,$type,"suadmin");
    }
}